<?php
/*
Plugin Name: Single Post 
Plugin URI: http://logicsbuffer.com
description: Single post subcategories show plugin by logics Buffer
Version: 1.0
Author: Mustafa Jamal
Author URI: http://mustafajamal.logicsbuffer.com
License: GPL2
*/
?>
<?php
	function show_sub_categories() {
		ob_start();
		global $post;
		
		// grab categories of current post
		$categories = get_the_category($post->ID);
		$parent_cat = $categories[0]->category_parent;
		$cat_name = get_cat_name( $parent_cat );
		//print_r($cat_name);

		if ($cat_name == 'Articles') {
			if ( is_active_sidebar( 'articles_single_page' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'articles_single_page' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'innerpage_1' ); ?>
				</div><!-- #common-sidebar -->
			<?php endif;
		}
		if ($cat_name == 'Reports') {
			if ( is_active_sidebar( 'reports_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'reports_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Interviews') {
			if ( is_active_sidebar( 'interviews_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'interviews_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Our Blog') {
			if ( is_active_sidebar( 'blog_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'blog_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Editorial') {
			if ( is_active_sidebar( 'editorial_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'editorial_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Public Voice') {
			if ( is_active_sidebar( 'publicvoice_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'publicvoice_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'In Pictures') {
			if ( is_active_sidebar( 'inpictures_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'inpictures_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}		
		if ($cat_name == 'In Videos') {
			if ( is_active_sidebar( 'invideos_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'invideos_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Stories') {
			if ( is_active_sidebar( 'stories_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'stories_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}	
		if ($cat_name == 'Add-ins') {
			if ( is_active_sidebar( 'add_ins_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'add_ins_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if(is_archive() || is_category()){
			
			// grab categories of current post
			$category_archive = get_queried_object();
			
			//print_r($category_archive);
			$parent_cat = $category_archive->parent;
			$cat_name = get_cat_name( $parent_cat );
			if ($cat_name == 'Articles') {
			if ( is_active_sidebar( 'innerpage_articles' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'innerpage_articles' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Reports') {
				if ( is_active_sidebar( 'innerpage_reports' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_reports' ); 
					?></div><?
				endif; 
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Interviews') {
				if ( is_active_sidebar( 'innerpage_interviews' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_interviews' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Our Blog') {
				if ( is_active_sidebar( 'innerpage_blog' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_blog' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Editorial') {
				if ( is_active_sidebar( 'innerpage_editorial' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_editorial' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Public Voice') {
				if ( is_active_sidebar( 'innerpage_publicvoice' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_publicvoice' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'In Pictures') {
				if ( is_active_sidebar( 'innerpage_inpictures' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_inpictures' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Stories') {
				if ( is_active_sidebar( 'innerpage_stories' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_stories' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Add-ins') {
				if ( is_active_sidebar( 'innerpage_addins' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_addins' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}			
			
			if ($cat_name == 'In Videos') {
				if ( is_active_sidebar( 'innerpage_videos' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_videos' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			}
			
		
		}
		
		return ob_get_clean();
	}
add_shortcode('show_subcat', 'show_sub_categories');
?>