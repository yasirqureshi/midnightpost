<?php
/**
 * OAuth section
 *
 * @package Reendex
 * @since 1.0.4
 */

namespace Reendex;

/**
 * Abraham Williams (abraham@abrah.am) http://abrah.am
 *
 * The first PHP Library to support OAuth for Twitter's REST API.
 *
 * @package Reendex
 */

if ( class_exists( 'TwitterOAuth' ) ) {
	return;
}

/* Load OAuth lib. You can find it at http://oauth.net */

/**
 * Generic exception class
 */
class OAuthException extends \Exception {
		// pass.
}
/**
 * The OAuthConsumer class contains the consumer key and secret.
 */
class OAuthConsumer {
	/**
	 * Consumer key
	 *
	 * @var string $key Consumer key
	 */
	public $key;
	/**
	 * Secret
	 *
	 * @var string $secret Consumer secret
	 */
	public $secret;
	/**
	 * Get parameter values.
	 *
	 * @param string $key OAuth key name.
	 * @param string $secret Secret used for OAuth signature.
	 * @param null   $callback_url URL to be returned to.
	 */
	function __construct( $key, $secret, $callback_url = null ) {
		$this->key = $key;
		$this->secret = $secret;
		$this->callback_url = $callback_url;
	}
	/**
	 * Return parameter values.
	 *
	 * @return string
	 */
	function __toString() {
		return "OAuthConsumer[key=$this->key,secret=$this->secret]";
	}
}

/**
 * OAuthToken class.
 */
class OAuthToken {
	/**
	 * Consumer key
	 *
	 * @var string $key Consumer key
	 */
	public $key;
	/**
	 * Secret
	 *
	 * @var string $secret Consumer secret
	 */
	public $secret;
	/**
	 * Construct
	 *
	 * @param string $key    Consumer key.
	 * @param string $secret Consumer secret.
	 *
	 * @return void
	 */
	function __construct( $key, $secret ) {
		$this->key = $key;
		$this->secret = $secret;
	}
	/**
	 * Generates the basic string serialization of a token that a server
	 * would respond to request_token and access_token calls with
	 */
	function to_string() {
		return 'oauth_token=' .
		OAuthUtil::urlencode_rfc3986( $this->key ) .
		'&oauth_token_secret=' .
		OAuthUtil::urlencode_rfc3986( $this->secret );
	}
	/**
	 * Return parameter values.
	 *
	 * @return string
	 */
	function __toString() {
		return $this->to_string();
	}
}

/**
 * A class for implementing a Signature Method
 * See section 9 ("Signing Requests") in the spec
 */
abstract class OAuthSignatureMethod {

	/**
	 * Needs to return the name of the Signature Method (ie HMAC-SHA1)
	 *
	 * @return string
	 */
	abstract public function get_name();

	/**
	 * Build up the signature
	 * NOTE: The output of this function MUST NOT be urlencoded.
	 * the encoding is handled in OAuthRequest when the final
	 * request is serialized
	 *
	 * @param OAuthRequest  $request Containing the request to make.
	 * @param OAuthConsumer $consumer The OAuth Consumer for this token.
	 * @param OAuthToken    $token OAuth token.
	 * @return string
	 */
	abstract public function build_signature( $request, $consumer, $token );

	/**
	 * Verifies that a given signature is correct
	 *
	 * @param OAuthRequest  $request Containing the request to make.
	 * @param OAuthConsumer $consumer The OAuth Consumer for this token.
	 * @param OAuthToken    $token OAuth token.
	 * @param string        $signature The signature.
	 * @return bool
	 */
	public function check_signature( $request, $consumer, $token, $signature ) {
		$built = $this->build_signature( $request, $consumer, $token );
		return $built == $signature;
	}
}

/**
 * The HMAC-SHA1 signature method uses the HMAC-SHA1 signature algorithm as defined in [RFC2104]
 * where the Signature Base String is the text and the key is the concatenated values (each first
 * encoded per Parameter Encoding) of the Consumer Secret and Token Secret, separated by an '&'
 * character (ASCII code 38) even if empty.
 *   - Chapter 9.2 ("HMAC-SHA1")
 */
class OAuthSignatureMethod_HMAC_SHA1 extends OAuthSignatureMethod {
	/**
	 * Needs to return the name of the Signature Method (ie HMAC-SHA1)
	 */
	function get_name() {
		return 'HMAC-SHA1';
	}
	/**
	 * Build up the signature
	 *
	 * @param OAuthRequest  $request Containing the request to make.
	 * @param OAuthConsumer $consumer The OAuth Consumer for this token.
	 * @param OAuthToken    $token OAuth token.
	 * @return string
	 */
	public function build_signature( $request, $consumer, $token ) {
		$base_string = $request->get_signature_base_string();
		$request->base_string = $base_string;
		$key_parts = array(
			$consumer->secret,
			( $token ) ? $token->secret : '',
			);
		$key_parts = OAuthUtil::urlencode_rfc3986( $key_parts );
		$key = implode( '&', $key_parts );
		return base64_encode( hash_hmac( 'sha1', $base_string, $key, true ) );
	}
}

/**
 * The PLAINTEXT method does not provide any security protection and SHOULD only be used
 * over a secure channel such as HTTPS. It does not use the Signature Base String.
 *   - Chapter 9.4 ("PLAINTEXT")
 */
class OAuthSignatureMethod_PLAINTEXT extends OAuthSignatureMethod {
	/**
	 * Needs to return the name of the Signature Method (ie PLAINTEXT)
	 */
	public function get_name() {
		return 'PLAINTEXT';
	}
	/**
	 * OAuth_signature is set to the concatenated encoded values of the Consumer Secret and
	 * Token Secret, separated by a '&' character (ASCII code 38), even if either secret is
	 * empty. The result MUST be encoded again.
	 *   - Chapter 9.4.1 ("Generating Signatures")
	 *
	 * Please note that the second encoding MUST NOT happen in the SignatureMethod, as
	 * OAuthRequest handles this!
	 *
	 * @param OAuthRequest  $request Containing the request to make.
	 * @param OAuthConsumer $consumer The OAuth Consumer for this token.
	 * @param OAuthToken    $token OAuth token.
	 */
	public function build_signature( $request, $consumer, $token ) {
		$key_parts = array(
		$consumer->secret,
		( $token ) ? $token->secret : '',
		);
			$key_parts = OAuthUtil::urlencode_rfc3986( $key_parts );
			$key = implode( '&', $key_parts );
			$request->base_string = $key;
			return $key;
	}
}

/**
 * The RSA-SHA1 signature method uses the RSASSA-PKCS1-v1_5 signature algorithm as defined in
 * [RFC3447] section 8.2 (more simply known as PKCS#1), using SHA-1 as the hash function for
 * EMSA-PKCS1-v1_5. It is assumed that the Consumer has provided its RSA public key in a
 * verified way to the Service Provider, in a manner which is beyond the scope of this
 * specification.
 *   - Chapter 9.3 ("RSA-SHA1")
 */
abstract class OAuthSignatureMethod_RSA_SHA1 extends OAuthSignatureMethod {
	/**
	 * Needs to return the name of the Signature Method (ie RSA-SHA1)
	 */
	public function get_name() {
		return 'RSA-SHA1';
	}

	/**
	 * Fetch the public CERT key for the signature
	 *
	 * @param OAuthRequest $request the OAuth Request.
	 * @return string public key
	 */
	protected abstract function fetch_public_cert( &$request );
	/**
	 * Fetch the private CERT key for the signature
	 *
	 * @param OAuthRequest $request the OAuth Request.
	 * @return string private key
	 */
	protected abstract function fetch_private_cert( &$request );
	/**
	 * Calculate the signature using RSA-SHA1
	 *
	 * @param OAuthRequest  $request The OAuth Request.
	 * @param OAuthConsumer $consumer The OAuth Consumer.
	 * @param OAuthToken    $token The OAuth Token.
	 * @return string
	 */
	public function build_signature( $request, $consumer, $token ) {
		$base_string = $request->get_signature_base_string();
		$request->base_string = $base_string;
			// Fetch the private key cert based on the request.
				$cert = $this->fetch_private_cert( $request );
			// Pull the private key ID from the certificate.
				$privatekeyid = openssl_get_privatekey( $cert );
			// Sign using the key.
				$ok = openssl_sign( $base_string, $signature, $privatekeyid );
			// Release the key resource.
				openssl_free_key( $privatekeyid );
				return base64_encode( $signature );
	}
	/**
	 * Verifies that a given signature is correct.
	 *
	 * @param OAuthRequest  $request The OAuth Request.
	 * @param OAuthConsumer $consumer The OAuth Consumer.
	 * @param OAuthToken    $token The OAuth Token.
	 * @param string        $signature The signature.
	 * @return string
	 */
	public function check_signature( $request, $consumer, $token, $signature ) {
		$decoded_sig = base64_decode( $signature );
		$base_string = $request->get_signature_base_string();
			// Fetch the public key cert based on the request.
				$cert = $this->fetch_public_cert( $request );
			// Pull the public key ID from the certificate.
				$publickeyid = openssl_get_publickey( $cert );
			// Check the computed signature against the one passed in the query.
				$ok = openssl_verify( $base_string, $decoded_sig, $publickeyid );
			// Release the key resource.
				openssl_free_key( $publickeyid );
				return 1 == $ok;
	}
}

/**
 * Class containing information for sending OAuth access requests.
 */
class OAuthRequest {

	/**
	 * Request parameters.
	 *
	 * @var array
	 */
	protected $parameters;

	/**
	 * Holds HTTP method
	 *
	 * @var string
	 */
	protected $http_method;

	/**
	 * Holds url
	 *
	 * @var string
	 */
	protected $http_url;

	/**
	 * For debug purposes.
	 */

	/**
	 * Generated base string for this request
	 *
	 * @var string
	 */
	public $base_string;

	/**
	 * Allowed version that we support with this library
	 *
	 * @var string
	 */
	public static $version = '1.0';

	/**
	 * Constructor
	 *
	 * @param string     $http_method The http method to use.
	 * @param string     $http_url The url.
	 * @param array|null $parameters The parameters.
	 */
	function __construct( $http_method, $http_url, $parameters = null ) {
		$parameters = ( $parameters ) ? $parameters : array();
		$parameters = array_merge( OAuthUtil::parse_parameters( parse_url( $http_url, PHP_URL_QUERY ) ), $parameters );
		$this->parameters = $parameters;
		$this->http_method = $http_method;
		$this->http_url = $http_url;
	}

	/**
	 * Pretty much a helper function to set up the request
	 *
	 * @param Consumer   $consumer The OAuth Consumer.
	 * @param Token      $token The OAuth Token.
	 * @param string     $http_method The http method to use.
	 * @param string     $http_url The url.
	 * @param array|null $parameters The parameters.
	 *
	 * @return Request
	 */
	public static function from_consumer_and_token( $consumer, $token, $http_method, $http_url, $parameters = null ) {
		$parameters = ( $parameters ) ? $parameters : array();
		$defaults = array(
			'oauth_version' => OAuthRequest::$version,
			'oauth_nonce' => OAuthRequest::generate_nonce(),
			'oauth_timestamp' => OAuthRequest::generate_timestamp(),
			'oauth_consumer_key' => $consumer->key,
			);
		if ( $token ) {
			$defaults['oauth_token'] = $token->key;
		}
		$parameters = array_merge( $defaults, $parameters );
		return new OAuthRequest( $http_method, $http_url, $parameters );
	}
	/**
	 * Add parameters to the request
	 *
	 * @param string $name The parameter name.
	 * @param string $value Value of the string.
	 * @param bool   $allow_duplicates Allow duplicate name + value entries.
	 */
	public function set_parameter( $name, $value, $allow_duplicates = true ) {
		if ( $allow_duplicates && isset( $this->parameters[ $name ] ) ) {
					// We have already added parameter(s) with this name, so add to the list.
			if ( is_scalar( $this->parameters[ $name ] ) ) {
					// This is the first duplicate, so transform scalar ( string )
					// into an array so we can add the duplicates.
					$this->parameters[ $name ] = array( $this->parameters[ $name ] );
			}
				$this->parameters[ $name ][] = $value;
		} else {
					$this->parameters[ $name ] = $value;
		}
	}
	/**
	 * Return the parameter value
	 *
	 * @param string $name The parameter name.
	 * @return string|null
	 */
	public function get_parameter( $name ) {
			return isset( $this->parameters[ $name ] ) ? $this->parameters[ $name ] : null;
	}
	/**
	 * Returns all the parameters
	 *
	 * @return array|null
	 */
	public function get_parameters() {
			return $this->parameters;
	}
	/**
	 * Removes a parameter.
	 *
	 * @param string $name The parameter name.
	 */
	public function unset_parameter( $name ) {
			unset( $this->parameters[ $name ] );
	}

	/**
	 * The request parameters, sorted and concatenated into a normalized string.
	 *
	 * @return string
	 */
	public function get_signable_parameters() {
			// Grab all parameters.
			$params = $this->parameters;
			// Remove oauth_signature if present
			// Ref: Spec: 9.1.1 ("The oauth_signature parameter MUST be excluded.").
		if ( isset( $params['oauth_signature'] ) ) {
					unset( $params['oauth_signature'] );
		}
			return OAuthUtil::build_http_query( $params );
	}
	/**
	 * Returns the base string of this request
	 *
	 * The base string defined as the method, the url
	 * and the parameters (normalized), each urlencoded
	 * and the concated with &.
	 */
	public function get_signature_base_string() {
			$parts = array(
				$this->get_normalized_http_method(),
				$this->get_normalized_http_url(),
				$this->get_signable_parameters(),
				);
			$parts = OAuthUtil::urlencode_rfc3986( $parts );
			return implode( '&', $parts );
	}
	/**
	 * Just uppercases the http method
	 */
	public function get_normalized_http_method() {
		return strtoupper( $this->http_method );
	}
	/**
	 * Parses the url and rebuilds it to be
	 * scheme://host/path
	 */
	public function get_normalized_http_url() {
		$parts = parse_url( $this->http_url );
		$scheme = ( isset( $parts['scheme'] ) ) ? $parts['scheme'] : 'http';
		$port = ( isset( $parts['port'] ) ) ? $parts['port'] : ( ( 'https' === $scheme ) ? '443' : '80' );
		$host = ( isset( $parts['host'] ) ) ? strtolower( $parts['host'] ) : '';
		$path = ( isset( $parts['path'] ) ) ? $parts['path'] : '';
		if ( ( 'https' === $scheme && '443' !== $port )
		|| ( 'http' === $scheme && '80' !== $port ) ) {
			$host = "$host:$port";
		}
		return "$scheme://$host$path";
	}
	/**
	 * Builds a url usable for a GET request
	 */
	public function to_url() {
		$post_data = $this->to_postdata();
		$out = $this->get_normalized_http_url();
		if ( $post_data ) {
			$out .= '?' . $post_data;
		}
		return $out;
	}
	/**
	 * Builds the data one would send in a POST request
	 */
	public function to_postdata() {
			return OAuthUtil::build_http_query( $this->parameters );
	}

	/**
	 * Return parameter values.
	 *
	 * @return string
	 */
	public function __toString() {
			return $this->to_url();
	}
	/**
	 * Public function for request signature creation
	 *
	 * @param string $signature_method Method used for signature.
	 * @param string $consumer The OAuth Consumer.
	 * @param string $token The OAuth Token.
	 */
	public function sign_request( $signature_method, $consumer, $token ) {
		$this->set_parameter(
			'oauth_signature_method',
			$signature_method->get_name(),
			false
		);
			$signature = $this->build_signature( $signature_method, $consumer, $token );
			$this->set_parameter( 'oauth_signature', $signature, false );
	}
	/**
	 * Build up the signature
	 *
	 * @param string $signature_method Method used for signature.
	 * @param string $consumer The OAuth Consumer.
	 * @param string $token The OAuth Token.
	 */
	public function build_signature( $signature_method, $consumer, $token ) {
			$signature = $signature_method->build_signature( $this, $consumer, $token );
			return $signature;
	}

	/**
	 * Util function: current timestamp
	 */
	private static function generate_timestamp() {
			return time();
	}
	/**
	 * Util function: current nonce
	 */
	private static function generate_nonce() {
			$mt = microtime();
			$rand = mt_rand();
			return md5( $mt . $rand ); // md5s look nicer than numbers.
	}
}

/**
 * Class to provide %OAuth utility methods.
 */
class OAuthUtil {
	/**
	 * Encode url
	 *
	 * @param string $input The string input.
	 *
	 * @return array|mixed|string
	 */
	public static function urlencode_rfc3986( $input ) {
		if ( is_array( $input ) ) {
			return array_map( array( 'Reendex\OAuthUtil', 'urlencode_rfc3986' ), $input );
		} elseif ( is_scalar( $input ) ) {
			return str_replace(
				'+',
				' ',
				str_replace( '%7E', '~', rawurlencode( $input ) )
			);
		} else {
			return '';
		}
	}

	/**
	 * This decode function isn't taking into consideration the above
	 * modifications to the encoding process. However, this method doesn't
	 * seem to be used anywhere so leaving it as is.
	 *
	 * @param string $string The source string.
	 *
	 * @return string
	 */
	public static function urldecode_rfc3986( $string ) {
		return urldecode( $string );
	}

	/**
	 * This function takes a input like a=b&a=c&d=e and returns the parsed
	 * parameters like this
	 * array('a' => array('b','c'), 'd' => 'e')
	 *
	 * @param string $input The string input.
	 *
	 * @return array
	 */
	public static function parse_parameters( $input ) {
		if ( ! isset( $input ) || ! $input ) {
			return array();
		}
		$pairs = explode( '&', $input );
		$parsed_parameters = array();
		foreach ( $pairs as $pair ) {
			$split = explode( '=', $pair, 2 );
			$parameter = OAuthUtil::urldecode_rfc3986( $split[0] );
			$value = isset( $split[1] ) ? OAuthUtil::urldecode_rfc3986( $split[1] ) : '';
			if ( isset( $parsed_parameters [ $parameter ] ) ) {
				// We have already recieved parameter(s) with this name, so add to the list
				// of parameters with this name.
				if ( is_scalar( $parsed_parameters[ $parameter ] ) ) {
					// This is the first duplicate, so transform scalar ( string ) into an array
					// so we can add the duplicates.
					$parsed_parameters[ $parameter ] = array( $parsed_parameters[ $parameter ] );
				}
				$parsed_parameters[ $parameter ][] = $value;
			} else {
				$parsed_parameters[ $parameter ] = $value;
			}
		}
		return $parsed_parameters;
	}

	/**
	 * Build HTTP query string
	 *
	 * Convert an array of parameter into query string
	 *
	 * @param array $params Array of parameters to convert.
	 * @return string Query string style parameters.
	 */
	public static function build_http_query( $params ) {
		if ( empty( $params ) ) {
			return '';
		}
		// Urlencode both keys and values.
		$keys = OAuthUtil::urlencode_rfc3986( array_keys( $params ) );
		$values = OAuthUtil::urlencode_rfc3986( array_values( $params ) );
		$params = array_combine( $keys, $values );
		// Parameters are sorted by name, using lexicographical byte value ordering.
		// Ref: Spec: 9.1.1 (1).
		uksort( $params, 'strcmp' );
		$pairs = array();
		foreach ( $params as $parameter => $value ) {
			if ( is_array( $value ) ) {
				// If two or more parameters share the same name, they are sorted by their value
				// June 12th, 2010 - changed to sort because of issue 164 by hidetaka.
				sort( $value, SORT_STRING );
				foreach ( $value as $duplicate_value ) {
					$pairs[] = $parameter . '=' . $duplicate_value;
				}
			} else {
				$pairs[] = $parameter . '=' . $value;
			}
		}
		// For each parameter, the name is separated from the corresponding value by an '=' character (ASCII code 61)
		// Each name-value pair is separated by an '&' character (ASCII code 38).
		return implode( '&', $pairs );
	}
}


