<?php
/**
 * Weather Widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_weather_widgets() {
	register_widget( 'reendex_Weather_Widget' );
}
	add_action( 'widgets_init', 'reendex_weather_widgets' );

	/**
	 * Core class used to implement the Weather widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_Weather_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'reendex_weather',
			'description'   => esc_html__( 'Reendex: Weather Widget', 'reendex' ),
		);
		$control_ops = array(
			'id_base' => 'weather-widget',
			);
		parent::__construct( 'weather-widget', esc_html( 'Reendex: Weather' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Weather widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current Weather widget instance.
	 */
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		?>
			
			<div id="weather" class="sidebar-weather <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
				<?php
				if ( $title ) {
						echo '<h4 class="widget-title weather-title">' . esc_html( $title ) . '</h4>';
				}
				?>
				<div class='panel'>
					<div class="widget-title">
						<span class='city' id='city'></span>
					</div>
					<div class="weather-card">
						<div class='weather'>
							<div class='temperature' id='temperature'>
								<div class='temp' id='temp'><i id='condition'></i> <span id='num'></span><a class='fahrenheit active' id='fahrenheit' href="#">&deg;F</a><span class='divider secondary'>|</span><a class='celsius' id='celsius' href="#">&deg;C</a></div>
							</div>
							<div class='group secondary'>
								<div id='haze' class="desc-text"></div>
								<div id='wind' class="desc-text"></div>
								<div id='humidity' class="desc-text"></div>
							</div>
							<div class='forecast' id='forecast'></div>							
						</div><!-- /.weather -->
					</div><!-- /.weather-card -->
				</div><!-- /.panel -->
			</div><!-- /#weather -->			
		
		<?php
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
		wp_reset_postdata();
	}

	/**
	 * Handles updating the settings for the current Weather widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['extclass']   = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Weather widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title'     => '',
			'extclass'  => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
		</p>
	<?php
	}
}
?>
