<?php
/**
 * Instagram widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function create_instagram_widget() {
	return register_widget( 'reendex_instagram_widget' );
}

	/**
	 * Core class used to implement the Instagram widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_Instagram_Widget extends WP_Widget {

	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor.
	 */
	function __construct() {

		$widget_ops = array(
			'classname'     => 'reendex-instagram-widget',
			'description'   => esc_html__( 'Reendex: Instagram Widget','reendex'
			),
		);
		$control_ops = array(
			'id_base' => 'reendex-instagram-widget',
			);
		parent::__construct( 'reendex-instagram-widget', esc_html( 'Reendex: Instagram' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Instagram widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current Instagram widget instance.
	 */
	function widget( $args, $instance ) {
		$title 		= ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$extclass   = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		$username   = ( ! empty( $instance['username'] ) ) ? $instance['username'] : 'instagram';
		$number     = empty( $instance['number'] ) ? 9 : $instance['number'];
		$limit      = empty( $instance['number'] ) ? 9 : $instance['number'];
		$size       = empty( $instance['size'] ) ? 'large' : $instance['size'];
		$link       = empty( $instance['link'] ) ? '' : $instance['link'];

		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		$target = '_self';
		if ( isset( $instance['target'] ) ) {
			$target = '_blank';
		}
		if ( '' != $username ) {
			$media_array = reendex_scrape_instagram( $username, $number );
			if ( is_wp_error( $media_array ) ) {
				echo wp_kses_post( $media_array->get_error_message() );
			} else {
				// filter for images only.
				$images_only = apply_filters( 'reendex_images_only', false );
				if ( $images_only ) {
					$media_array = array_filter( $media_array, array( $this, 'images_only' ) );
				}
				// slice list down to required limit.
				$media_array = array_slice( $media_array, 0, $limit );
				?>
				<div class="reendex-instagram-widget <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
				<?php
				if ( $title ) {
					echo '<h4 class="widget-title">' . esc_html( $title ) . '</h4>';
				}
				?>					
					<ul class="instagram-widget">
						<?php
						foreach ( $media_array as $item ) {
							$short_caption = wp_trim_words( $item['description'], 5, '' );
							$short_caption = preg_replace( '/[^A-Za-z0-9?! ]/','', $short_caption );
							echo '
								<li>
									<a href="' . esc_url( $item['link'] ) . '" target="' . esc_attr( $target ) . '">
										<img src="' . esc_url( $item[ $size ] ) . '" alt="' . esc_attr( $short_caption ) . '" title="' . esc_attr( $item['description'] ) . '">
									</a>
								</li>
							';
						}
						?>
					</ul><!-- /.instagram-widget -->
				</div><!-- /.reendex-instagram-widget -->
				<?php
			}
		} // End if().
		$linkclass = apply_filters( 'reendex_link_class', 'clear' );
		if ( '' != $link ) {
			?>
			<div class="view-more aligncenter <?php echo esc_attr( $linkclass ); ?>">
				<a href="//instagram.com/<?php echo esc_attr( trim( $username ) ); ?>"  rel="me" target="<?php echo esc_attr( $target ); ?>"><?php echo wp_kses_post( $link ); ?></a>
			</div>
			<?php
		}
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
	}

	/**
	 * Handles updating the settings for the current Instagram widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']      = sanitize_text_field( $new_instance['title'] );
		$instance['username']   = sanitize_text_field( $new_instance['username'] );
		$instance['number']     = ! absint( $new_instance['number'] ) ? 6 : $new_instance['number'];
		$instance['size']       = sanitize_key( $new_instance['size'] );
		$instance['target']     = intval( $new_instance['target'] );
		$instance['link']       = sanitize_text_field( $new_instance['link'] );
		$instance['extclass']   = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Instagram widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title'     => esc_html__( 'Instagram', 'reendex' ),
			'username'  => '',
			'number'    => 6,
			'size'      => 'large',
			'target'    => 0,
			'link'      => '',
			'extclass'  => '',
		);
		$username   = isset( $instance['username'] ) ? $instance['username'] : '';
		$target     = isset( $instance['target'] ) ? $instance['target'] : '';
		$extclass   = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		$number     = absint( $instance['number'] );
		$instance   = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" /> 
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( 'Username:','reendex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" type="text" value="<?php echo esc_attr( $username ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">
				<?php esc_html_e( 'Number Of Photos:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" value="<?php echo isset( $instance['number'] ) ? esc_attr( $instance['number'] ) : '6'; ?>" />
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>" type="checkbox"  name="<?php echo esc_attr( $this->get_field_name( 'target' ) ); ?>" value="1" <?php echo '1' == $target ? 'checked="checked"' : ''; ?> />
			<label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>"><?php esc_html_e( 'Check to Open Link in new Tab/Window','reendex' ); ?></label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php esc_html_e( 'Link text', 'reendex' ); ?>:
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['link'] ); ?>" /></label></p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
		</p>			
		<?php
	}


	/**
	 * Media item type.
	 *
	 * @param object $media_item The media item type.
	 */
	function images_only( $media_item ) {
		if ( 'image' == $media_item['type'] ) {
			return true;
		}
		return false;
	}
}
add_action( 'widgets_init', 'create_instagram_widget' );
