<?php
/**
 * Follow Us widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_follow_us_widgets() {
	register_widget( 'reendex_Follow_Us_Widget' );
}
	add_action( 'widgets_init', 'reendex_follow_us_widgets' );

	/**
	 * Core class used to implement the Follow Us widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_Follow_Us_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'follow_us',
			'description'   => esc_html__( 'Reendex: Follow Us Widget','reendex' ),
			);
		$control_ops = array(
			'id_base' => 'follow_us-widget',
			);
		parent::__construct( 'follow_us-widget', esc_html__( 'Reendex: Follow Us','reendex' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Follow Us widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current Follow Us widget instance.
	 */
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		$facebook = isset( $instance['facebook'] ) ? $instance['facebook'] : '';
		$twitter = isset( $instance['twitter'] ) ?  $instance['twitter'] : '';
		$google = isset( $instance['google'] ) ?  $instance['google'] : '';
		$pinterest = isset( $instance['pinterest'] ) ?  $instance['pinterest'] : '';
		$tumblr = isset( $instance['tumblr'] ) ?  $instance['tumblr'] : '';
		$instagram = isset( $instance['instagram'] ) ?  $instance['instagram'] : '';
		$vimeo = isset( $instance['vimeo'] ) ?  $instance['vimeo'] : '';
		$dribbble = isset( $instance['dribbble'] ) ?  $instance['dribbble'] : '';
		$youtube = isset( $instance['youtube'] ) ?  $instance['youtube'] : '';
		$flickr = isset( $instance['flickr'] ) ?  $instance['flickr'] : '';
		$linkedin = isset( $instance['linkedin'] ) ?  $instance['linkedin'] : '';
		$github = isset( $instance['github'] ) ?  $instance['github'] : '';
		$skype = isset( $instance['skype'] ) ?  $instance['skype'] : '';
		$feed = isset( $instance['feed'] ) ?  $instance['feed'] : '';
		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		?>
		<li class="follow_us menu-social-icons <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
			<?php
			if ( $title ) {
				echo '<h4 class="widget-title">' . esc_attr( $title ) . '</h4>';
			}
			?>
		<ul>
			<?php if ( $facebook ) { ?>
				<li><a href="<?php echo esc_url( $instance['facebook'] ); ?>" class="facebook" title="<?php esc_attr_e( 'Facebook', 'reendex' );?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
			<?php } ?>
			
			<?php if ( $twitter ) { ?>
				<li><a href="<?php echo esc_url( $instance['twitter'] ); ?>" class="twitter" title="<?php esc_attr_e( 'Twitter', 'reendex' );?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
			<?php } ?>
			
			<?php if ( $google ) { ?>
				<li><a href="<?php echo esc_url( $instance['google'] ); ?>" class="google-plus" title="<?php esc_attr_e( 'Google+', 'reendex' );?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			<?php } ?>
			
			<?php if ( $pinterest ) { ?>
				<li><a href="<?php echo esc_url( $instance['pinterest'] ); ?>" class="pinterest" title="<?php esc_attr_e( 'Pinterest', 'reendex' );?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
			<?php } ?>
			
			<?php if ( $tumblr ) { ?>
				<li><a href="<?php echo esc_url( $instance['tumblr'] ); ?>" class="tumblr" title="<?php esc_attr_e( 'Tumblr', 'reendex' );?>" target="_blank"><i class="fa fa-tumblr"></i></a></li>
			<?php } ?>
			
			<?php if ( $instagram ) { ?>
				<li><a href="<?php echo esc_url( $instance['instagram'] ); ?>" class="instagram" title="<?php esc_attr_e( 'Instagram', 'reendex' );?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
			<?php } ?>
			
			<?php if ( $vimeo ) { ?>
				<li><a href="<?php echo esc_url( $instance['vimeo'] ); ?>" class="vimeo" title="<?php esc_attr_e( 'Vimeo', 'reendex' );?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
			<?php } ?>
			
			<?php if ( $dribbble ) { ?>
				<li><a href="<?php echo esc_url( $instance['dribbble'] ); ?>" class="dribbble" title="<?php esc_attr_e( 'Dribbble', 'reendex' );?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
			<?php } ?>
			
			<?php if ( $youtube ) { ?>
				<li><a href="<?php echo esc_url( $instance['youtube'] ); ?>" class="youtube" title="<?php esc_attr_e( 'Youtube', 'reendex' );?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
			<?php } ?>
			
			<?php if ( $flickr ) { ?>
				<li><a href="<?php echo esc_url( $instance['flickr'] ); ?>" class="flickr" title="<?php esc_attr_e( 'flickr', 'reendex' );?>" target="_blank"><i class="fa fa-flickr"></i></a></li>
			<?php } ?>
			
			<?php if ( $linkedin ) { ?>
				<li><a href="<?php echo esc_url( $instance['linkedin'] ); ?>" class="linkedin" title="<?php esc_attr_e( 'LinkedIn', 'reendex' );?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			<?php } ?>

			<?php if ( $github ) { ?>
				<li><a href="<?php echo esc_url( $instance['github'] ); ?>" class="github" title="<?php esc_attr_e( 'Github', 'reendex' );?>" target="_blank"><i class="fa fa-github-alt"></i></a></li>
			<?php } ?>

			<?php if ( $skype ) { ?>
				<li><a href="skype:<?php echo esc_url( $instance['skype'] ); ?>?userinfo" class="skype" title="<?php esc_attr_e( 'Skype Profile Name', 'reendex' );?>" target="_blank"><i class="fa fa-skype"></i></a></li>
			<?php } ?>

			<?php if ( $feed ) { ?>
				<li><a href="<?php echo esc_url( $instance['feed'] ); ?>" class="rss" title="<?php esc_attr_e( 'Feed', 'reendex' );?>" target="_blank"><i class="fa fa-rss"></i></a></li>
			<?php } ?>
		</ul>
		</li><!-- /.follow_us menu-social-icons -->	
		<?php
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
	}

	/**
	 * Handles updating the settings for the current Follow Us widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']          = sanitize_text_field( $new_instance['title'] );
		$instance['facebook'] = isset( $new_instance['facebook'] ) ?  esc_url_raw( $new_instance['facebook'] ) : '';
		$instance['twitter'] = isset( $new_instance['twitter'] ) ? esc_url_raw( $new_instance['twitter'] ) : '';
		$instance['google'] = isset( $new_instance['google'] ) ? esc_url_raw( $new_instance['google'] ) : '';
		$instance['pinterest'] = isset( $new_instance['pinterest'] ) ? esc_url_raw( $new_instance['pinterest'] ) : '';
		$instance['tumblr'] = isset( $new_instance['tumblr'] ) ?  esc_url_raw( $new_instance['tumblr'] ) : '';
		$instance['instagram'] = isset( $new_instance['instagram'] ) ?  esc_url_raw( $new_instance['instagram'] ) : '';
		$instance['vimeo'] = isset( $new_instance['vimeo'] ) ?  esc_url_raw( $new_instance['vimeo'] ) : '';
		$instance['dribbble'] = isset( $new_instance['dribbble'] ) ?  esc_url_raw( $new_instance['dribbble'] ) : '';
		$instance['youtube'] = isset( $new_instance['youtube'] ) ?  esc_url_raw( $new_instance['youtube'] ) : '';
		$instance['flickr'] = isset( $new_instance['flickr'] ) ?  esc_url_raw( $new_instance['flickr'] ) : '';
		$instance['linkedin'] = isset( $new_instance['linkedin'] ) ?  esc_url_raw( $new_instance['linkedin'] ) : '';
		$instance['github'] = isset( $new_instance['github'] ) ?  esc_url_raw( $new_instance['github'] ) : '';
		$instance['skype'] = isset( $new_instance['skype'] ) ?  esc_url_raw( $new_instance['skype'] ) : '';
		$instance['feed'] = isset( $new_instance['feed'] ) ?  esc_url_raw( $new_instance['feed'] ) : '';
		$instance['extclass']       = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Follow Us widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title' 	=> esc_html__( 'Follow Us', 'reendex' ),
			'facebook'  => '',
			'twitter'   => '',
			'google'    => '',
			'pinterest' => '',
			'tumblr'    => '',
			'instagram' => '',
			'vimeo'     => '',
			'dribbble'  => '',
			'youtube'   => '',
			'flickr'    => '',
			'linkedin'  => '',
			'github'    => '',
			'skype'     => '',
			'feed'      => '',
			'extclass' 	=> '',
		);
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php esc_attr_e( 'Facebook Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" value="<?php echo esc_attr( $instance['facebook'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php esc_attr_e( 'Twitter Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" value="<?php echo esc_attr( $instance['twitter'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'google' ) ); ?>"><?php esc_attr_e( 'Google+ Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'google' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'google' ) ); ?>" value="<?php echo esc_attr( $instance['google'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ); ?>"><?php esc_attr_e( 'Pinterest Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pinterest' ) ); ?>" value="<?php echo esc_attr( $instance['pinterest'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ); ?>"><?php esc_attr_e( 'Tumblr Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tumblr' ) ); ?>" value="<?php echo esc_attr( $instance['tumblr'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>"><?php esc_attr_e( 'Instagram Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" value="<?php echo esc_attr( $instance['instagram'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'vimeo' ) ); ?>"><?php esc_attr_e( 'Vimeo Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'vimeo' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'vimeo' ) ); ?>" value="<?php echo esc_attr( $instance['vimeo'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'dribbble' ) ); ?>"><?php esc_attr_e( 'Dribbble Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'dribbble' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'dribbble' ) ); ?>" value="<?php echo esc_attr( $instance['dribbble'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php esc_attr_e( 'YouTube Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" value="<?php echo esc_attr( $instance['youtube'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'flickr' ) ); ?>"><?php esc_attr_e( 'Flickr Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'flickr' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'flickr' ) ); ?>" value="<?php echo esc_attr( $instance['flickr'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>"><?php esc_attr_e( 'LinkedIn Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ); ?>" value="<?php echo esc_attr( $instance['linkedin'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'github' ) ); ?>"><?php esc_attr_e( 'Github Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'github' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'github' ) ); ?>" value="<?php echo esc_attr( $instance['github'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'skype' ) ); ?>"><?php esc_attr_e( 'Skype User ID: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'skype' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'skype' ) ); ?>" value="<?php echo esc_attr( $instance['skype'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'feed' ) ); ?>"><?php esc_attr_e( 'RSS Feed Link: ', 'reendex' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'feed' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'feed' ) ); ?>" value="<?php echo esc_attr( $instance['feed'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_attr_e( 'Widget area class','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
		</p>    	
	<?php
	}
}
?>
