<?php
/**
 * Control - Section.
 *
 * @package Reendex
 */

if ( class_exists( 'WP_Customize_Control' ) ) {
	/**
	 * Custom Control for Customizer Section Settings.
	 *
	 * @since Reendex 1.0
	 *
	 * @see WP_Customize_Control
	 */
	class Reendex_Customizer_Section extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 *
		 * @since  1.0
		 * @access public
		 * @var    string
		 */
		public $type = 'section';

		 /**
		  * Loads the framework scripts/styles.
		  *
		  * @since  1.0
		  * @access public
		  * @return void
		  */
		public function enqueue() {
			wp_enqueue_style( 'reendex-control-section', trailingslashit( get_template_directory_uri() ) . '/css/reendex-control-section.css', '', time() );
		}

		/**
		 * Render the control to be displayed in the Customizer.
		 *
		 * @since Reendex 1.0
		 */
		public function render_content() {
		?>
			<div class="customize-section-description">
				<span class="title customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			</div><!-- /.customize-section-description -->
		<?php
		}
	}
} // End if().
