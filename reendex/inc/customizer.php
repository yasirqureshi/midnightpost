<?php
/**
 * Reendex Theme Customizer
 *
 * @package Reendex
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function reendex_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	// Remove some sections.
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'colors' );

	/*defaults options*/
	$defaults = reendex_get_default_theme_options();

	$imagepath = get_template_directory_uri() . '/img/';

	$priority = 1;

	// Ad Sizes.
	$get_ad_size = array(
		'auto'      => esc_html__( 'Auto', 'reendex' ),
		'hide'      => esc_html__( 'Hide', 'reendex' ),
		// Standard Ad Sizes.
		'120x240'   => esc_html__( '120 x 240 - Vertical banner', 'reendex' ),
		'120x600'   => esc_html__( '120 x 600 - Skyscraper', 'reendex' ),
		'125x125'   => esc_html__( '125 x 125 - Square Button', 'reendex' ),
		'160x600'   => esc_html__( '160 x 600 - Wide skyscraper', 'reendex' ),
		'180x150'   => esc_html__( '180 x 150 - Small rectangle', 'reendex' ),
		'200x200'   => esc_html__( '200 x 200 - Small square', 'reendex' ),
		'234x60'    => esc_html__( '234 x 60 - Half banner', 'reendex' ),
		'250x250'   => esc_html__( '250 x 250 - Square', 'reendex' ),
		'300x250'   => esc_html__( '300 x 250 - Medium rectangle', 'reendex' ),
		'300x600'   => esc_html__( '300 x 600 - Large Skyscraper', 'reendex' ),
		'320x50'    => esc_html__( '320 x 50 - Mobile Leaderboard', 'reendex' ),
		'320x100'   => esc_html__( '320 x 100 - Large mobile banner', 'reendex' ),
		'336x280'   => esc_html__( '336 x 280 - Large rectangle', 'reendex' ),
		'468x60'    => esc_html__( '468 x 60 - Banner', 'reendex' ),
		'728x90'    => esc_html__( '728 x 90 - Leaderboard', 'reendex' ),
		'970x50'    => esc_html__( '970 x 50 - Billboard', 'reendex' ),
		'970x90'    => esc_html__( '970 x 90 - Large leaderboard', 'reendex' ),
		// Regional Ad Sizes.
		'240x400'   => esc_html__( '240 x 400 - Vertical rectangle', 'reendex' ),
		'250x360'   => esc_html__( '250 x 360 - Triple widescreen', 'reendex' ),
		'580x400'   => esc_html__( '580 x 400 - Netboard', 'reendex' ),
		'750x100'   => esc_html__( '750 x 100 - Billboard', 'reendex' ),
		'750x200'   => esc_html__( '750 x 200 - Double billboard', 'reendex' ),
		'750x300'   => esc_html__( '750 x 300 - Triple billboard', 'reendex' ),
		'930x180'   => esc_html__( '930 x 180 - Top banner', 'reendex' ),
		'980x120'   => esc_html__( '980 x 120 - Panorama', 'reendex' ),
		// Link Unit Ads.
		'120x90'    => esc_html__( '120 x 90 - Vertical Small', 'reendex' ),
		'160x90'    => esc_html__( '160 x 90 - Vertical Medium', 'reendex' ),
		'180x90'    => esc_html__( '180 x 90 - Vertical Large', 'reendex' ),
		'200x90'    => esc_html__( '200 x 90 - Vertical X-Large', 'reendex' ),
		'468x15'    => esc_html__( '468 x 15 - Horizontal Medium', 'reendex' ),
		'728x15'    => esc_html__( '728 x 15 - Horizontal Large', 'reendex' ),
	);

	/**
	 * Add panels
	 */
	$wp_customize->add_panel( 'reendex_header', array(
		'priority'          => $priority ++,
		'capability'        => 'edit_theme_options',
		'theme_supports'    => '',
		'title'             => esc_html__( 'Header', 'reendex' ),
		'description'       => esc_html__( 'Description of what this panel does.', 'reendex' ),
	) );
	$wp_customize->add_panel( 'reendex_options_panel', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Theme Options', 'reendex' ),
	) );
	$wp_customize->add_panel( 'reendex_responsive_menu', array(
		'priority'      => 140,
		'title'         => esc_html__( 'Responsive Menu', 'reendex' ),
		'description'   => esc_html__( 'Responsive Menu Options', 'reendex' ),
	) );
	$wp_customize->add_panel( 'reendex_advertising_panel', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Advertising Options', 'reendex' ),
	) );

	$priority = 1;

	/**
	 * Add sections
	 */

	// Header Options section.
	$wp_customize->add_section( 'reendex_header_options_section', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Header Layout Options', 'reendex' ),
		'panel'         => 'reendex_header',
	) );

	// Header Layout Options.
	$wp_customize->add_setting( 'reendex_header_layout', array(
		'default'           => 'reendex-header-layout-one',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_header_layout', array(
		'label'         => esc_html__( 'Header Layout Options', 'reendex' ),
		'description'   => 'Please save and refresh to see the changes',
		'section'       => 'reendex_header_options_section',
		'settings'      => 'reendex_header_layout',
		'choices'       => array(
			'reendex-header-layout-one'     => get_template_directory_uri() . '/img/header-style1.png',
			'reendex-header-layout-two'     => get_template_directory_uri() . '/img/header-style2.png',
			'reendex-header-layout-three'   => trailingslashit( get_template_directory_uri() ) . '/img/header-style3.png',
			'reendex-header-layout-four'    => trailingslashit( get_template_directory_uri() ) . '/img/header-style4.png',
			'reendex-header-layout-five'    => trailingslashit( get_template_directory_uri() ) . '/img/header-style5.png',
			'reendex-header-layout-six'     => trailingslashit( get_template_directory_uri() ) . '/img/header-style6.png',
		),
	) ) );

	// Logo section.
	$wp_customize->add_section( 'reendex_logo_section', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Logo', 'reendex' ),
		'description'   => esc_html__( 'Upload a logo to replace the default site name and description in the header', 'reendex' ),
		'panel'         => 'reendex_header',
	) );
	$wp_customize->add_setting( 'reendex_logo', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_logo', array(
		'label'     => esc_html__( 'Upload Logo', 'reendex' ),
		'section'   => 'reendex_logo_section',
		'settings'  => 'reendex_logo',
	) ) );

	// Top Bar section.
	$wp_customize->add_section( 'reendex_topbar_section', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Top Header Settings', 'reendex' ),
		'description'   => esc_html__( 'Edit Top Header here', 'reendex' ),
		'panel'         => 'reendex_header',
	) );

	// Setting - Email.
	$wp_customize->add_setting( 'reendex_top_email', array(
		'default'            => esc_html__( 'info@domain.com', 'reendex' ),
		'capability'         => 'edit_theme_options',
		'sanitize_callback'  => 'sanitize_text_field',
	));
	$wp_customize->add_control( 'reendex_top_email', array(
		'label'         => esc_html__( 'Email', 'reendex' ),
		'description'   => esc_html__( 'Provide your email address to be displayed on top header section.', 'reendex' ),
		'section'       => 'reendex_topbar_section',
		'type'          => 'text',
	) );

	// Setting - Phone.
	$wp_customize->add_setting( 'reendex_top_phone', array(
		'default'           => esc_html__( '+00 (123) 456 7890', 'reendex' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_top_phone', array(
		'label'             => esc_html__( 'Phone', 'reendex' ),
		'description'       => esc_html__( 'Provide your phone number to be displayed on top header section.', 'reendex' ),
		'section'           => 'reendex_topbar_section',
		'type'              => 'text',
	) );

	// Setting - Address.
	$wp_customize->add_setting( 'reendex_top_address', array(
		'default'           => esc_html__( '121 King Street', 'reendex' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_top_address', array(
		'label'             => esc_html__( 'Address', 'reendex' ),
		'description'       => esc_html__( 'Provide your address to be displayed on top header section.', 'reendex' ),
		'section'           => 'reendex_topbar_section',
		'type'              => 'text',
	) );

	// Show/Hide Social media icons.
	$wp_customize->add_setting( 'reendex_topbar_socialmedia_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_topbar_socialmedia_show', array(
		'type' 		=> 'switch',
		'settings'  => 'reendex_topbar_socialmedia_show',
		'section'   => 'reendex_topbar_section',
		'label'     => esc_html__( 'Choose whether to display the Social Media icons on top header', 'reendex' ),
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Show/Hide Search form.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_show_search_in_header]', array(
		'default'           => $defaults['reendex_show_search_in_header'],
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_show_search_in_header]', array(
		'label'     => esc_html__( 'Enable Search Form', 'reendex' ),
		'section'   => 'reendex_topbar_section',
		'type'      => 'checkbox',
	) );

	// General Menu Options section.
	$wp_customize->add_section( 'reendex_menu_options', array(
		'priority'      => $priority ++,
		'title'         => esc_html__( 'Menu Options', 'reendex' ),
		'panel'         => 'reendex_header',
	) );

	// Sticky menu.
	$wp_customize->add_setting( 'reendex_sticky_menu', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_sticky_menu', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Enable sticky menu', 'reendex' ),
		'section'   => 'reendex_menu_options',
		'settings'  => 'reendex_sticky_menu',
		'choices'   => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
	) ) );

	// Menu Hover effect.
	$wp_customize->add_setting( 'reendex_hover_menu', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_hover_menu', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Enable Hover effect on Menu', 'reendex' ),
		'section'   => 'reendex_menu_options',
		'settings'  => 'reendex_hover_menu',
		'choices'   => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
	) ) );

	// Date and Time display enable/disable.
	$wp_customize->add_section('reendex_date_display_section', array(
		'title' => esc_html__( 'Date and Time', 'reendex' ),
		'panel' => 'reendex_header',
	) );
	$wp_customize->add_setting('reendex_date_display', array(
		'default'           => 0,
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );
	$wp_customize->add_control('reendex_date_display', array(
		'type'      => 'checkbox',
		'label'     => esc_html__( 'Check this box to show the date and time on all pages',  'reendex' ),
		'section'   => 'reendex_date_display_section',
		'settings'  => 'reendex_date_display',
	) );
	// Date format.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_date_format]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_date_format'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_date_format]', array(
		'label'         => esc_html__( 'Date format','reendex' ),
		'type'          => 'text',
		'section'       => 'reendex_date_display_section',
		'settings'      => 'reendex_theme_options[reendex_date_format]',
		'description'   => wp_kses(
			/* translators: %s is a link with text "Developer Codex" and URL https://codex.wordpress.org/Formatting_Date_and_Time */
			sprintf( __( "Please set your date format. For more details about this format, please refer to <a href='%s' target='_blank'>Developer Codex</a>.", 'reendex' ), 'https://codex.wordpress.org/Formatting_Date_and_Time' ), wp_kses_allowed_html()
		),
	) );
	// Time type.
	$wp_customize->add_setting( 'reendex_time_type', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default' => 'client-side',
	) );
	$wp_customize->add_control( 'reendex_time_type', array(
		'label' => esc_html__( 'Time type', 'reendex' ),
		'description' => esc_html__( 'Choose the time type', 'reendex' ),
		'choices' => array(
			'client-side' => esc_html__( 'Live time type', 'reendex' ),
			'server-side' => esc_html__( 'WordPress time type', 'reendex' ),
		),
		'type' => 'select',
		'section'  => 'reendex_date_display_section',
	) );
	// Time format.
	$wp_customize->add_setting( 'reendex_time_format', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default' => '12',
	) );
	$wp_customize->add_control( 'reendex_time_format', array(
		'label' => esc_html__( 'Time format', 'reendex' ),
		'description' => esc_html__( 'Choose between 12 and 24 hours system.', 'reendex' ),
		'choices' => array(
			'24' => esc_html__( '24-hour', 'reendex' ),
			'12' => esc_html__( '12-hour', 'reendex' ),
		),
		'type'      => 'select',
		'section'   => 'reendex_date_display_section',
	) );

	/**
	* General options panel section
	*/
	$wp_customize->add_section( 'reendex_general_options', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'General options', 'reendex' ),
		'panel'     => 'reendex_options_panel',
	) );

	// General Options Heading.
	$wp_customize->add_setting( 'reendex_general_options_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_general_options_heading', array(
		'label'     => esc_html__( 'General options', 'reendex' ),
		'section'   => 'reendex_general_options',
		'settings'  => 'reendex_general_options_heading',
	) ) );

	// Sticky sidebar.
	$wp_customize->add_setting( 'reendex_sticky_sidebar', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_sticky_sidebar', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Enable sticky sidebar on Single Posts, Search Page and Archive Page.', 'reendex' ),
		'section'   => 'reendex_general_options',
		'settings'  => 'reendex_sticky_sidebar',
		'type'	  	=> 'select',
		'choices'   => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
	) ) );

	// Switch to updated date.
	$wp_customize->add_setting( 'reendex_updated_date', array(
		'default'           => '',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );
	$wp_customize->add_control('reendex_updated_date', array(
		'type'      => 'checkbox',
		'label'     => esc_html__( 'Replace posts published date with last updated date', 'reendex' ),
		'section'   => 'reendex_general_options',
	) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_banner_show_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_banner_show_heading', array(
		'label'     => esc_html__( 'Header Banner Options for Default Page', 'reendex' ),
		'section'   => 'reendex_general_options',
		'settings'  => 'reendex_banner_show_heading',
	) ) );

	// Banner Section.
	$wp_customize->add_setting( 'reendex_banner_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_banner_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner on Default Page.', 'reendex' ),
		'section'   => 'reendex_general_options',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'       => 'reendex_general_options',
	) ) );

	$wp_customize->add_setting( 'reendex_banner_title', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_banner_title', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Header Banner Title and Breadcrumbs, on Default Page.', 'reendex' ),
		'section'   => 'reendex_general_options',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	$wp_customize->add_setting( 'reendex_banner_title_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_banner_title_text_color', array(
		'label'     => esc_html__( 'Title text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_general_options',
	) );

	$wp_customize->add_setting( 'reendex_banner_breadcrumbs_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#000',
	) );
	$wp_customize->add_control( 'reendex_banner_breadcrumbs_bg_color', array(
		'label'     => esc_html__( 'Breadcrumbs background color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_general_options',
	) );
	$wp_customize->add_setting( 'reendex_banner_breadcrumbs_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_banner_breadcrumbs_text_color', array(
		'label'     => esc_html__( 'Breadcrumbs text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_general_options',
	) );

	// Show Category label.
	$wp_customize->add_setting( 'reendex_video_show_category', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_video_show_category', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Category label on video post image.', 'reendex' ),
		'section' 	=> 'reendex_general_options',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	/**
	* Typography panel section
	*/
	$wp_customize->add_section( 'reendex_typography', array(
		'priority'      => $priority ++,
		'title'         => esc_html__( 'Typography', 'reendex' ),
		'panel'         => 'reendex_options_panel',
		'description'   => esc_html__( 'Google Fonts can be found here: https://fonts.google.com ( Just enter fonts name like Roboto )', 'reendex' ),
	) );
	// Body fonts family.
	$wp_customize->add_setting( 'reendex_body_font_family', array(
		'default'           => 'Roboto',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_body_font_family', array(
		'label'     => esc_html__( 'Body font', 'reendex' ),
		'section'   => 'reendex_typography',
		'type'      => 'text',
	) );
	// Fonts family option.
	$wp_customize->add_setting( 'reendex_title_font_option', array(
		'default'           => 'Roboto Condensed',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_title_font_option', array(
		'label'     => esc_html__( 'Title font', 'reendex' ),
		'section'   => 'reendex_typography',
		'type'      => 'text',
	) );
	// Fonts size.
	$wp_customize->add_setting( 'reendex_body_font_size', array(
		'default'           => '14',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_body_font_size', array(
		'label'         => esc_html__( 'Body font size', 'reendex' ),
		'section'       => 'reendex_typography',
		'type'          => 'number',
		'description'   => 'Please save and refresh to see the changes',
	) );
	$wp_customize->add_setting( 'reendex_first_nav_font_size', array(
		'default'           => '14',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_first_nav_font_size', array(
		'label'     => esc_html__( 'Menu font size', 'reendex' ),
		'section'   => 'reendex_typography',
		'type'      => 'number',
	) );
	$wp_customize->add_setting( 'reendex_first_nav_font_weight', array(
		'default'           => '500',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_first_nav_font_weight', array(
		'label'     => esc_html__( 'Menu font weight', 'reendex' ),
		'section'   => 'reendex_typography',
		'type'      => 'number',
	) );
	$wp_customize->add_setting( 'reendex_bottom_nav_font_size', array(
		'default'           => '14',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_bottom_nav_font_size', array(
		'label'     => esc_html__( 'Bottom Menu font size', 'reendex' ),
		'section'   => 'reendex_typography',
		'type'      => 'number',
	) );
	$wp_customize->add_setting( 'reendex_bottom_nav_font_weight', array(
		'default'           => '400',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_bottom_nav_font_weight', array(
		'label'     => esc_html__( 'Bottom Menu font weight', 'reendex' ),
		'section'   => 'reendex_typography',
		'type'      => 'number',
	) );

	/**
	 * Colors section
	 */
	$wp_customize->add_section( 'reendex_colors', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Colors', 'reendex' ),
		'panel'     => 'reendex_options_panel',
	) );
	// Theme Primary.
	$wp_customize->add_setting( 'reendex_theme_color', array(
		'default'           => '#d4000e',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'reendex_theme_color', array(
		'label'     => esc_html__( 'Theme Primary color', 'reendex' ),
		'section'   => 'reendex_colors',
	) ) );
	// Theme Secondary.
	$wp_customize->add_setting( 'reendex_theme_secondary_color', array(
		'default'           => '#be000c',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'reendex_theme_secondary_color', array(
		'label'     => esc_html__( 'Theme Secondary color', 'reendex' ),
		'section'   => 'reendex_colors',
	) ) );
	// Hover Color.
	$wp_customize->add_setting( 'reendex_hover_background_color', array(
		'default'           => '#e4000d',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'reendex_hover_background_color', array(
		'label'     => esc_html__( 'Theme Hover background color', 'reendex' ),
		'section'   => 'reendex_colors',
	) ) );
	// Hover Color.
	$wp_customize->add_setting( 'reendex_hover_color', array(
		'default'           => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'reendex_hover_color', array(
		'label'     => esc_html__( 'Theme Hover color', 'reendex' ),
		'section'   => 'reendex_colors',
	) ) );

	/**
	 * Preloader section
	 */
	$wp_customize->add_section( 'reendex_activate_loader_section', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Activate Preloader', 'reendex' ),
		'panel'     => 'reendex_options_panel',
	) );
	$wp_customize->add_setting( 'reendex_activate_loader', array(
		'default'           => 1,
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'reendex_activate_loader', array(
		'type'      => 'checkbox',
		'label' 	=> esc_html__( 'Activate Loader', 'reendex' ),
		'settings'  => 'reendex_activate_loader',
		'section' 	=> 'reendex_activate_loader_section',
	) );
	$wp_customize->add_setting( 'reendex_preloader_bg', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_preloader_bg', array(
		'label'         => esc_html__( 'Preloader background color', 'reendex' ),
		'description'   => '',
		'type'          => 'color',
		'section'       => 'reendex_activate_loader_section',
	) );
	$wp_customize->add_setting( 'reendex_preloader_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#777',
	) );
	$wp_customize->add_control( 'reendex_preloader_color', array(
		'label'         => esc_html__( 'Preloader color', 'reendex' ),
		'description'   => '',
		'type'          => 'color',
		'section'       => 'reendex_activate_loader_section',
	) );

	/**
	* Single Post Page.
	*/
	$wp_customize->add_section( 'reendex_single_page_section', array(
		'priority'          => $priority ++,
		'capability'        => 'edit_theme_options',
		'theme_supports'    => '',
		'title'             => esc_html__( 'Single Post Page Options', 'reendex' ),
		'description'       => '',
		'panel'             => 'reendex_options_panel',
	) );

	// Post Layout Heading.
	$wp_customize->add_setting( 'reendex_post_layout_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_post_layout_heading', array(
		'label'     => esc_html__( 'Post Layout options', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_post_layout_heading',
	) ) );

	// Single Page Layout Settings.
	$wp_customize->add_setting( 'reendex_single_post_layout', array(
		'default'           => 'rightsidebar',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_single_post_layout', array(
		'label' => esc_html__( 'Select Single Post Layout. This Layout will be reflected in all Single Posts, unless unique layout is set for a specific post.', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_single_post_layout',
		'choices'   => array(
			'leftsidebar'   => trailingslashit( get_template_directory_uri() ) . '/img/left-sidebar.png',
			'rightsidebar'  => trailingslashit( get_template_directory_uri() ) . '/img/right-sidebar.png',
			'nosidebar'     => trailingslashit( get_template_directory_uri() ) . '/img/no-sidebar.png',
		),
	) ) );

	// Breaking News Settings.
	$wp_customize->add_setting( 'reendex_single_breaking_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_single_breaking_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Breaking News Ticker on Single Posts (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Post Meta Heading.
	$wp_customize->add_setting( 'reendex_post_meta_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_post_meta_heading', array(
		'label'     => esc_html__( 'Post Meta options', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_post_meta_heading',
	) ) );
	// Disable Single Posts Meta.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_single_disable_postmeta]', array(
		'default'           => $defaults['reendex_single_disable_postmeta'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_single_disable_postmeta]', array(
		'label' 	=> esc_html__( 'Enable Post Meta on Single Posts (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_single_disable_postmeta]',
		'type' 		=> 'checkbox',
	) );

	// Show Post Author Avatar.
	$wp_customize->add_setting( 'reendex_avatar_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_avatar_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Show Post Author Avatar', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// User avatar size.
	$wp_customize->add_setting( 'reendex_avatar_size', array(
		'default'           => '28',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_avatar_size', array(
		'label'         => esc_html__( 'Post Author Avatar size', 'reendex' ),
		'section'       => 'reendex_single_page_section',
		'type'          => 'number',
	) );

	// Show Post Meta Date.
	$wp_customize->add_setting( 'reendex_date_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_date_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Show Post Date', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Post Date Format.
	$wp_customize->add_setting( 'reendex_post_date_format', array(
		'default'           => 'default',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_post_date_format', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select which date format you want to use for single post meta.', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'choices'   => array(
			'ago'      => esc_html__( 'Relative Date/Time Format (ago)', 'reendex' ),
			'default'  => esc_html__( 'WordPress Default Date Format', 'reendex' ),
			'custom'   => esc_html__( 'Custom Date Format', 'reendex' ),
		),
	) );

	$wp_customize->add_setting( 'reendex_custom_date_format', array(
		'type'              => 'theme_mod',
		'default'           => 'Y/m/d',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_custom_date_format', array(
		'label'      => esc_html__( 'Custom Date format','reendex' ),
		'type'       => 'text',
		'section'    => 'reendex_single_page_section',
		'settings'   => 'reendex_custom_date_format',
		'description'   => wp_kses(
			/* translators: %s is a link with text "Developer Codex" and URL https://codex.wordpress.org/Formatting_Date_and_Time */
			sprintf( __( "Please set your date format. For more detail about this format, please refer to <a href='%s' target='_blank'>Developer Codex</a>.", 'reendex' ), 'https://codex.wordpress.org/Formatting_Date_and_Time' ), wp_kses_allowed_html()
		),
		'active_callback' => 'reendex_date_format',
	) );

	// Show Published Date, Updated Date or both.
	$wp_customize->add_setting( 'reendex_post_date', array(
		'default'           => 'updated',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_post_date', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Show Published Date, Updated Date or both', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'choices'   => array(
			'published' => esc_html__( 'Show Published Date', 'reendex' ),
			'updated'   => esc_html__( 'Show Updated Date', 'reendex' ),
			'both'      => esc_html__( 'Show both Published Date and Updated Date', 'reendex' ),
		),
	) );

	// Social Share Heading.
	$wp_customize->add_setting( 'reendex_social_share_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_social_share_heading', array(
		'label'     => esc_html__( 'Social Share options', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_social_share_heading',
	) ) );

	// Social Settings.
	$wp_customize->add_setting( 'reendex_share_display_all', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_display_all', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Social Share on Single Posts (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	$wp_customize->add_setting( 'reendex_share_facebook', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_facebook', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Facebook (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_twitter', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_twitter', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Twitter (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_google', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_google', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Google Plus (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_linkedin', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_linkedin', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Linkedin (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_pinterest', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_pinterest', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Pinterest (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_tumblr', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_tumblr', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Tumblr (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_reddit', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_reddit', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Reddit (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_stumbleupon', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_stumbleupon', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Stumbleupon (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_digg', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_digg', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Digg (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_vk', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_vk', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Vk (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_share_pocket', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_share_pocket', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Pocket (This will be reflected in all Single Posts, unless otherwise is set for a specific post)', 'reendex' ),
		'section' 	=> 'reendex_single_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Add Related posts Heading.
	$wp_customize->add_setting( 'reendex_related_posts_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_related_posts_heading', array(
		'label'     => esc_html__( 'Related Posts from the same author', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_related_posts_heading',
	) ) );

	// Related posts from the same author.
	$wp_customize->add_setting( 'reendex_author_related_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_author_related_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Related Posts by the same author on Single Post.','reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_author_related_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Related posts by the same author content styles.
	$wp_customize->add_setting( 'reendex_author_related_content_style', array(
		'default'           => '3',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_author_related_content_style', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select Related Posts Content Style', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'choices'   => array(
			'1'  => esc_html__( 'Style 1', 'reendex' ),
			'2'  => esc_html__( 'Style 2', 'reendex' ),
			'3'  => esc_html__( 'Style 3', 'reendex' ),
		),
	) );

	$wp_customize->add_setting( 'reendex_author_related_title_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_author_related_title_show', array(
		'type'      => 'switch',
		'label'     => esc_html__( 'Choose whether to display the Title of Related Posts by the same author.','reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_author_related_title_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_author_related_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_author_related_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_author_related_title]', array(
		'label'     => esc_html__( 'Title','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_author_related_title]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_author_related_subtitle]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_author_related_subtitle'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_author_related_subtitle]', array(
		'label'      => esc_html__( 'Subtitle','reendex' ),
		'type'       => 'text',
		'section'    => 'reendex_single_page_section',
		'settings'   => 'reendex_theme_options[reendex_author_related_subtitle]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_author_related_number]', array(
		'default'           => $defaults['reendex_author_related_number'],
		'type'              => 'theme_mod',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_author_related_content_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_author_related_content_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_author_related_content_length]', array(
		'label'     => esc_html__( 'Author related posts content length', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_author_related_content_length]',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_author_related_number]', array(
		'label'         => esc_html__( 'Number of items to display', 'reendex' ),
		'section'       => 'reendex_single_page_section',
		'type'          => 'number',
		'description'   => 'Please save and refresh to see the changes',
	) );

	// Add Related posts by category / tags Heading.
	$wp_customize->add_setting( 'reendex_related_posts_cat_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_related_posts_cat_heading', array(
		'label'     => esc_html__( 'Related Posts by category / tags', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_related_posts_cat_heading',
	) ) );

	// Related posts by category / tags.
	$wp_customize->add_setting( 'reendex_related_category_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_related_category_show', array(
		'label'     => esc_html__( 'Choose whether to display Related Posts by category / tags, on single post.','reendex' ),
		'type'      => 'switch',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_related_category_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_related_posts', array(
		'default'           => 'categories',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_related_posts_sanitize',
	) );
	$wp_customize->add_control( 'reendex_related_posts', array(
		'type'      => 'radio',
		'label'     => esc_html__( 'Related Posts Must Be Shown As:', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_related_posts',
		'choices'   => array(
			'categories'    => esc_html__( 'Related Posts By Categories', 'reendex' ),
			'tags'          => esc_html__( 'Related Posts By Tags', 'reendex' ),
		),
	) );
	$wp_customize->add_setting( 'reendex_related_category_title_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_related_category_title_show', array(
		'type'      => 'switch',
		'label'     => esc_html__( 'Choose whether to display the Title of Related Posts by category / tags.','reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_related_category_title_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_category_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_related_category_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_category_title]', array(
		'label'     => esc_html__( 'Title','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_related_category_title]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_category_subtitle]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_related_category_subtitle'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_category_subtitle]', array(
		'label'      => esc_html__( 'Subtitle','reendex' ),
		'type'       => 'text',
		'section'    => 'reendex_single_page_section',
		'settings'   => 'reendex_theme_options[reendex_related_category_subtitle]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_content_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_related_content_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_content_length]', array(
		'label'      => esc_html__( 'Related Posts Content Length', 'reendex' ),
		'type'       => 'number',
		'section'    => 'reendex_single_page_section',
		'settings'   => 'reendex_theme_options[reendex_related_content_length]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_category_number]', array(
		'default'           => $defaults['reendex_related_category_number'],
		'type'              => 'theme_mod',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_category_number]', array(
		'label'         => esc_html__( 'Number of related posts by category or tags', 'reendex' ),
		'section'       => 'reendex_single_page_section',
		'type'          => 'number',
		'description'   => 'Please save and refresh to see the changes',
	) );

	// Add Random Posts Heading.
	$wp_customize->add_setting( 'reendex_random_posts_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_random_posts_heading', array(
		'label'     => esc_html__( 'Random Posts options', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_random_posts_heading',
	) ) );

	// Random posts settings.
	$wp_customize->add_setting( 'reendex_random_posts_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_random_posts_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Random Posts on single post template.','reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_random_posts_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_random_posts_per_page]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_random_posts_per_page'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_random_posts_per_page]', array(
		'label'     => esc_html__( 'The number of items to display', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_random_posts_per_page]',
	) );
	$wp_customize->add_setting( 'reendex_random_posts_title_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_random_posts_title_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Title of Random Posts.','reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_random_posts_title_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_random_posts_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_random_posts_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_random_posts_title]', array(
		'label'      => esc_html__( 'Title','reendex' ),
		'type'       => 'text',
		'section'    => 'reendex_single_page_section',
		'settings'   => 'reendex_theme_options[reendex_random_posts_title]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_random_posts_subtitle]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_random_posts_subtitle'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_random_posts_subtitle]', array(
		'label'     => esc_html__( 'Subtitle','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_random_posts_subtitle]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_random_posts_content_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_random_posts_content_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_random_posts_content_length]', array(
		'label'     => esc_html__( 'Random Posts Content Length', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_theme_options[reendex_random_posts_content_length]',
	) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_single_header_banner_heading', array(
		'type'                => 'option',
		'capability'          => 'edit_theme_options',
		'theme_supports'      => '',
		'default'             => '',
		'transport'           => 'refresh',
		'sanitize_callback'   => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_single_header_banner_heading', array(
		'label'     => esc_html__( 'Header Banner options', 'reendex' ),
		'section'   => 'reendex_single_page_section',
		'settings'  => 'reendex_single_header_banner_heading',
	) ) );

	// Single Page Banner Section.
	$wp_customize->add_setting( 'reendex_single_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_single_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'       => 'reendex_single_page_section',
	) ) );

	$wp_customize->add_setting( 'reendex_single_banner_title_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_single_banner_title_text_color', array(
		'label'     => esc_html__( 'Title text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_single_page_section',
	) );

	$wp_customize->add_setting( 'reendex_single_banner_breadcrumbs_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#000',
	) );
	$wp_customize->add_control( 'reendex_single_banner_breadcrumbs_bg_color', array(
		'label'     => esc_html__( 'Breadcrumbs background color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_single_page_section',
	) );
	$wp_customize->add_setting( 'reendex_single_banner_breadcrumbs_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_single_banner_breadcrumbs_text_color', array(
		'label'     => esc_html__( 'Breadcrumbs text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_single_page_section',
	) );

	/**
	 * Archive Page Section
	 */
	$wp_customize->add_section( 'reendex_archive_pages', array(
		'priority'   => $priority ++,
		'title'      => esc_html__( 'Archive Page Options', 'reendex' ),
		'panel'      => 'reendex_options_panel',
	) );

	/**
	 * Archive Page Layout Settings
	 */

	$wp_customize->add_setting( 'reendex_archive_page_layout', array(
		'default'           => 'rightsidebar',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_archive_page_layout', array(
		'label'     => esc_html__( 'Select Archive Page Layout', 'reendex' ),
		'section'   => 'reendex_archive_pages',
		'settings'  => 'reendex_archive_page_layout',
		'choices'   => array(
			'leftsidebar'   => trailingslashit( get_template_directory_uri() ) . '/img/left-sidebar.png',
			'rightsidebar'  => trailingslashit( get_template_directory_uri() ) . '/img/right-sidebar.png',
			'nosidebar'     => trailingslashit( get_template_directory_uri() ) . '/img/no-sidebar.png',
		),
	) ) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_archive_banner_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_archive_banner_heading', array(
		'label'     => esc_html__( 'Header Banner Options', 'reendex' ),
		'section'   => 'reendex_archive_pages',
		'settings'  => 'reendex_archive_banner_heading',
	) ) );

	// Banner Section.
	$wp_customize->add_setting( 'reendex_archive_banner_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_archive_banner_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner on Archive Pages.', 'reendex' ),
		'section'   => 'reendex_archive_pages',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_archive_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_archive_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'       => 'reendex_archive_pages',
	) ) );

	$wp_customize->add_setting( 'reendex_archive_banner_title', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_archive_banner_title', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Header Banner Title and Breadcrumbs on Archive Pages.', 'reendex' ),
		'section'   => 'reendex_archive_pages',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Archive Page Options Heading.
	$wp_customize->add_setting( 'reendex_archive_options_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_archive_options_heading', array(
		'label'     => esc_html__( 'Archive Pages Options', 'reendex' ),
		'section'   => 'reendex_archive_pages',
		'settings'  => 'reendex_archive_options_heading',
	) ) );

	// Archive Pages content styles.
	$wp_customize->add_setting( 'reendex_archive_content_style', array(
		'default'           => '4',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_archive_content_style', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select Archive Pages Content Style', 'reendex' ),
		'section'   => 'reendex_archive_pages',
		'choices'   => array(
			'1'  => esc_html__( 'Style 1', 'reendex' ),
			'2'  => esc_html__( 'Style 2', 'reendex' ),
			'3'  => esc_html__( 'Style 3', 'reendex' ),
			'4'  => esc_html__( 'Style 4', 'reendex' ),
		),
	) );

	// Archive Pages Read More Text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_readmore_text]', array(
		'default'           => $defaults['reendex_readmore_text'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_readmore_text]', array(
		'label'       	=> esc_html__( 'Read More', 'reendex' ),
		'description'   => esc_html__( 'Change the "Read More" text here.', 'reendex' ),
		'section'     	=> 'reendex_archive_pages',
		'settings'    	=> 'reendex_theme_options[reendex_readmore_text]',
		'type'         	=> 'text',
	) );

	// Disable Archive Pages Meta.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_disable_postmeta]', array(
		'default'           => $defaults['reendex_disable_postmeta'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_disable_postmeta]', array(
		'label' 	=> esc_html__( 'Enable Post Meta on Archive Pages', 'reendex' ),
		'section' 	=> 'reendex_archive_pages',
		'settings'  => 'reendex_theme_options[reendex_disable_postmeta]',
		'type' 		=> 'checkbox',
	) );

	// Disable Archive Pages Footer.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_disable_post_footer]', array(
		'default'           => $defaults['reendex_disable_post_footer'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_disable_post_footer]', array(
		'label' 	=> esc_html__( 'Enable Post Footer on Archive Pages', 'reendex' ),
		'section' 	=> 'reendex_archive_pages',
		'settings'  => 'reendex_theme_options[reendex_disable_post_footer]',
		'type' 	    => 'checkbox',
	) );

	// Breaking News Settings.
	$wp_customize->add_setting( 'reendex_archive_breaking_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_archive_breaking_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Breaking News Ticker on Archive Pages.', 'reendex' ),
		'section' 	=> 'reendex_archive_pages',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Featured images on Archives Pages.
	$wp_customize->add_setting( 'reendex_image_archives', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_image_archives', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Featured image on Archive Pages.','reendex' ),
		'section' 	=> 'reendex_archive_pages',
		'settings'  => 'reendex_image_archives',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Show Category label.
	$wp_customize->add_setting( 'reendex_archive_show_category', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_archive_show_category', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Category label on image.', 'reendex' ),
		'section' 	=> 'reendex_archive_pages',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Custom excerpt length.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_excerpt_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_excerpt_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_excerpt_length]', array(
		'label'     => esc_html__( 'Excerpt word count on blog and archives', 'reendex' ),
		'type'      => 'number',
		'section' 	=> 'reendex_archive_pages',
		'settings'  => 'reendex_theme_options[reendex_excerpt_length]',
	) );

	/**
	 * Tag Archive Section
	 */
	$wp_customize->add_section( 'reendex_tag_archive', array(
		'priority'   => $priority ++,
		'title'      => esc_html__( 'Tag Archive Options', 'reendex' ),
		'panel'      => 'reendex_options_panel',
	) );

	/**
	 * Tag Archive Layout Settings
	 */

	$wp_customize->add_setting( 'reendex_tag_archive_layout', array(
		'default'           => 'rightsidebar',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_tag_archive_layout', array(
		'label'     => esc_html__( 'Select Tag Archive Layout', 'reendex' ),
		'section'   => 'reendex_tag_archive',
		'settings'  => 'reendex_tag_archive_layout',
		'choices'   => array(
			'leftsidebar'   => trailingslashit( get_template_directory_uri() ) . '/img/left-sidebar.png',
			'rightsidebar'  => trailingslashit( get_template_directory_uri() ) . '/img/right-sidebar.png',
			'nosidebar'     => trailingslashit( get_template_directory_uri() ) . '/img/no-sidebar.png',
		),
	) ) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_tag_banner_show_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_tag_banner_show_heading', array(
		'label'     => esc_html__( 'Header Banner Options', 'reendex' ),
		'section' 	=> 'reendex_tag_archive',
		'settings'  => 'reendex_tag_banner_show_heading',
	) ) );

	// Banner Section.
	$wp_customize->add_setting( 'reendex_tag_banner_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_tag_banner_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner on Tag Archive pages.', 'reendex' ),
		'section' 	=> 'reendex_tag_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_tag_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_tag_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section' 	    => 'reendex_tag_archive',
	) ) );

	$wp_customize->add_setting( 'reendex_tag_banner_title', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_tag_banner_title', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Header Banner Title and Breadcrumbs on Tag Archive pages.', 'reendex' ),
		'section' 	=> 'reendex_tag_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Tag Archive Page Options Heading.
	$wp_customize->add_setting( 'reendex_tag_options_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_tag_options_heading', array(
		'label'     => esc_html__( 'Tag Archive Pages Options', 'reendex' ),
		'section' 	=> 'reendex_tag_archive',
		'settings'  => 'reendex_tag_options_heading',
	) ) );

	// Tag Archive content styles.
	$wp_customize->add_setting( 'reendex_tag_content_style', array(
		'default'           => '4',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_tag_content_style', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select Tag Archive Content Style', 'reendex' ),
		'section' 	=> 'reendex_tag_archive',
		'choices'   => array(
			'1'  => esc_html__( 'Style 1', 'reendex' ),
			'2'  => esc_html__( 'Style 2', 'reendex' ),
			'3'  => esc_html__( 'Style 3', 'reendex' ),
			'4'  => esc_html__( 'Style 4', 'reendex' ),
		),
	) );

	// Post Read More Text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_tag_readmore_text]', array(
		'default'           => $defaults['reendex_tag_readmore_text'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_tag_readmore_text]', array(
		'label'       	=> esc_html__( 'Read More', 'reendex' ),
		'description'   => esc_html__( 'Change the "Read More" text here.', 'reendex' ),
		'section'     	=> 'reendex_tag_archive',
		'settings'    	=> 'reendex_theme_options[reendex_tag_readmore_text]',
		'type'         	=> 'text',
	) );

	// Disable Post Meta.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_tag_disable_postmeta]', array(
		'default'           => $defaults['reendex_tag_disable_postmeta'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_tag_disable_postmeta]', array(
		'label' 	=> esc_html__( 'Enable Post Meta', 'reendex' ),
		'section'   => 'reendex_tag_archive',
		'settings'  => 'reendex_theme_options[reendex_tag_disable_postmeta]',
		'type' 		=> 'checkbox',
	) );

	// Disable Post Footer.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_tag_disable_post_footer]', array(
		'default'           => $defaults['reendex_tag_disable_post_footer'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_tag_disable_post_footer]', array(
		'label' 	=> esc_html__( 'Enable Post Footer', 'reendex' ),
		'section'   => 'reendex_tag_archive',
		'settings'  => 'reendex_theme_options[reendex_tag_disable_post_footer]',
		'type' 	    => 'checkbox',
	) );

	// Breaking News Settings.
	$wp_customize->add_setting( 'reendex_tag_archive_breaking_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_tag_archive_breaking_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Breaking News Ticker on Tag Archive.', 'reendex' ),
		'section'   => 'reendex_tag_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Featured images on tag archives.
	$wp_customize->add_setting( 'reendex_image_tag_archives', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_image_tag_archives', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Featured image on Tag Archives.','reendex' ),
		'section'   => 'reendex_tag_archive',
		'settings'  => 'reendex_image_tag_archives',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Show Category label.
	$wp_customize->add_setting( 'reendex_tag_show_category', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_tag_show_category', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Category label on image.', 'reendex' ),
		'section' 	=> 'reendex_tag_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Custom excerpt length on tag archives.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_tag_excerpt_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_tag_excerpt_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_tag_excerpt_length]', array(
		'label'     => esc_html__( 'Excerpt character count on Tag Archives', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_tag_archive',
		'settings'  => 'reendex_theme_options[reendex_tag_excerpt_length]',
	) );

	/**
	 * Category Archive Section
	 */
	$wp_customize->add_section( 'reendex_category_archive', array(
		'priority'   => $priority ++,
		'title'      => esc_html__( 'Category Archive Options', 'reendex' ),
		'panel'      => 'reendex_options_panel',
	) );

	/**
	 * Category Archive Layout Settings
	 */

	$wp_customize->add_setting( 'reendex_category_archive_layout', array(
		'default'           => 'rightsidebar',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_category_archive_layout', array(
		'label'     => esc_html__( 'Select Category Archive Layout', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_category_archive_layout',
		'choices'   => array(
			'leftsidebar'   => trailingslashit( get_template_directory_uri() ) . '/img/left-sidebar.png',
			'rightsidebar'  => trailingslashit( get_template_directory_uri() ) . '/img/right-sidebar.png',
			'nosidebar'     => trailingslashit( get_template_directory_uri() ) . '/img/no-sidebar.png',
		),
	) ) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_category_banner_show_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_category_banner_show_heading', array(
		'label'     => esc_html__( 'Header Banner Options', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_category_banner_show_heading',
	) ) );
	// Banner Section.
	$wp_customize->add_setting( 'reendex_category_banner_show', array(
		'default'           => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_category_banner_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner on Category Archive pages.', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_category_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_category_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'   => 'reendex_category_archive',
	) ) );

	$wp_customize->add_setting( 'reendex_category_banner_title', array(
		'default'           => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_category_banner_title', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Header Banner Title and Breadcrumbs on Category Archive pages.', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Category Archive Page Options Heading.
	$wp_customize->add_setting( 'reendex_category_options_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_category_options_heading', array(
		'label'     => esc_html__( 'Category Archive Pages Options', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_category_options_heading',
	) ) );

	// Category Archive content styles.
	$wp_customize->add_setting( 'reendex_category_content_style', array(
		'default'           => '4',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_category_content_style', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select Category Archive Content Style', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'choices'   => array(
			'1'  => esc_html__( 'Style 1', 'reendex' ),
			'2'  => esc_html__( 'Style 2', 'reendex' ),
			'3'  => esc_html__( 'Style 3', 'reendex' ),
			'4'  => esc_html__( 'Style 4', 'reendex' ),
		),
	) );

	// Post Read More Text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_category_readmore_text]', array(
		'default'           => $defaults['reendex_category_readmore_text'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_category_readmore_text]', array(
		'label'       	=> esc_html__( 'Read More', 'reendex' ),
		'description'   => esc_html__( 'Change the "Read More" text here.', 'reendex' ),
		'section'       => 'reendex_category_archive',
		'settings'    	=> 'reendex_theme_options[reendex_category_readmore_text]',
		'type'         	=> 'text',
	) );

	// Disable Post Meta.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_category_disable_postmeta]', array(
		'default'           => $defaults['reendex_category_disable_postmeta'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_category_disable_postmeta]', array(
		'label' 	=> esc_html__( 'Enable Post Meta', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_theme_options[reendex_category_disable_postmeta]',
		'type' 		=> 'checkbox',
	) );

	// Disable Post Footer.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_category_disable_post_footer]', array(
		'default'           => $defaults['reendex_category_disable_post_footer'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_category_disable_post_footer]', array(
		'label' 	=> esc_html__( 'Enable Post Footer', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_theme_options[reendex_category_disable_post_footer]',
		'type' 	    => 'checkbox',
	) );

	// Breaking News Settings.
	$wp_customize->add_setting( 'reendex_category_archive_breaking_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_category_archive_breaking_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Breaking News Ticker on Category Archive.', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Featured images on category archives.
	$wp_customize->add_setting( 'reendex_image_category_archives', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_image_category_archives', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Featured image on Category Archives.','reendex' ),
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_image_category_archives',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Show Category label.
	$wp_customize->add_setting( 'reendex_category_show_category', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_category_show_category', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Category label on image.', 'reendex' ),
		'section'   => 'reendex_category_archive',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Custom excerpt length on category archives.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_category_excerpt_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_category_excerpt_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_category_excerpt_length]', array(
		'label'     => esc_html__( 'Excerpt character count on Category Archives', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_category_archive',
		'settings'  => 'reendex_theme_options[reendex_category_excerpt_length]',
	) );

	/**
	* Blog Page Options
	*/
	$wp_customize->add_section( 'reendex_blog_page_section', array(
		'priority'          => $priority ++,
		'capability'        => 'edit_theme_options',
		'theme_supports'    => '',
		'title'             => esc_html__( 'Blog Page Options', 'reendex' ),
		'description'       => '',
		'panel'             => 'reendex_options_panel',
	) );

	/**
	 * Blog Page Layout Settings
	 */

	$wp_customize->add_setting( 'reendex_blog_page_layout', array(
		'default'           => 'rightsidebar',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_blog_page_layout', array(
		'label'     => esc_html__( 'Select Blog Page Layout', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_blog_page_layout',
		'choices'   => array(
			'leftsidebar'   => trailingslashit( get_template_directory_uri() ) . '/img/left-sidebar.png',
			'rightsidebar'  => trailingslashit( get_template_directory_uri() ) . '/img/right-sidebar.png',
			'nosidebar'     => trailingslashit( get_template_directory_uri() ) . '/img/no-sidebar.png',
		),
	) ) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_blog_banner_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_blog_banner_heading', array(
		'label'     => esc_html__( 'Header Banner Options', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_blog_banner_heading',
	) ) );

	// Blog Page Banner Settings.
	$wp_customize->add_setting( 'reendex_blog_banner_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_blog_banner_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner on Blog Page.', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_blog_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_blog_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'       => 'reendex_blog_page_section',
	) ) );

	$wp_customize->add_setting( 'reendex_blog_banner_title_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_blog_banner_title_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner Title and Subtitle.', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	$wp_customize->add_setting( 'reendex_blog_banner_title_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_blog_banner_title_text_color', array(
		'label'     => esc_html__( 'Title text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_blog_page_section',
	) );

	$wp_customize->add_setting( 'reendex_blog_banner_subtitle_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#000',
	) );
	$wp_customize->add_control( 'reendex_blog_banner_subtitle_bg_color', array(
		'label'     => esc_html__( 'Subtitle background color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_blog_page_section',
	) );
	$wp_customize->add_setting( 'reendex_blog_banner_subtitle_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_blog_banner_subtitle_text_color', array(
		'label'     => esc_html__( 'Subtitle text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_blog_page_section',
	) );

	// Blog Page Banner Title.
	$wp_customize->add_setting( 'reendex_blog_banner_title', array(
		'type'              => 'option',
		'default'           => esc_html__( 'News from reality', 'reendex' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_blog_banner_title', array(
		'label'     => esc_html__( 'Banner Title', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_blog_page_section',
	) );
	// Blog Page Banner Subtitle.
	$wp_customize->add_setting( 'reendex_blog_banner_subtitle', array(
		'type'              => 'option',
		'default'           => esc_html__( 'Look for more news from our own sources', 'reendex' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_blog_banner_subtitle', array(
		'label'     => esc_html__( 'Banner Subtitle', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_blog_page_section',
	) );
	// Subtitle button link.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_banner_subtitle_link]', array(
		'sanitize_callback' => 'esc_url_raw',
		'capability'        => 'edit_theme_options',
		'default'           => $defaults['reendex_banner_subtitle_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_banner_subtitle_link]', array(
		'label'     => esc_html__( 'Subtitle button link', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'type'      => 'text',
	) );

	// Blog Page Options Heading.
	$wp_customize->add_setting( 'reendex_blog_options_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_blog_options_heading', array(
		'label'     => esc_html__( 'Blog Page Options', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_blog_options_heading',
	) ) );

	// Blog Page content styles.
	$wp_customize->add_setting( 'reendex_blog_content_style', array(
		'default'           => '4',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_blog_content_style', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select Blog Page Content Style', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'choices'   => array(
			'1'  => esc_html__( 'Style 1', 'reendex' ),
			'2'  => esc_html__( 'Style 2', 'reendex' ),
			'3'  => esc_html__( 'Style 3', 'reendex' ),
			'4'  => esc_html__( 'Style 4', 'reendex' ),
		),
	) );

	// Post Read More Text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_blog_readmore_text]', array(
		'default'           => $defaults['reendex_blog_readmore_text'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_blog_readmore_text]', array(
		'label'       	=> esc_html__( 'Read More', 'reendex' ),
		'description'   => esc_html__( 'Change the "Read More" text here.', 'reendex' ),
		'section'       => 'reendex_blog_page_section',
		'settings'    	=> 'reendex_theme_options[reendex_blog_readmore_text]',
		'type'         	=> 'text',
	) );

	// Disable Post Meta.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_blog_disable_postmeta]', array(
		'default'           => $defaults['reendex_blog_disable_postmeta'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_blog_disable_postmeta]', array(
		'label' 	=> esc_html__( 'Enable Post Meta', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_theme_options[reendex_blog_disable_postmeta]',
		'type' 		=> 'checkbox',
	) );

	// Disable Post Footer.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_blog_disable_post_footer]', array(
		'default'           => $defaults['reendex_blog_disable_post_footer'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_blog_disable_post_footer]', array(
		'label' 	=> esc_html__( 'Enable Post Footer', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_theme_options[reendex_blog_disable_post_footer]',
		'type' 	    => 'checkbox',
	) );

	// Breaking News Settings.
	$wp_customize->add_setting( 'reendex_blog_breaking_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_blog_breaking_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Breaking News Ticker on Blog Page.', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Featured images on blog page.
	$wp_customize->add_setting( 'reendex_post_image_archives', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_post_image_archives', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Featured image on Blog Page.','reendex' ),
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_post_image_archives',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Show Category label.
	$wp_customize->add_setting( 'reendex_blog_show_category', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_blog_show_category', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Category label on image.', 'reendex' ),
		'section'   => 'reendex_blog_page_section',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Custom excerpt length on tag archives.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_blog_excerpt_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_blog_excerpt_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_blog_excerpt_length]', array(
		'label'     => esc_html__( 'Excerpt character count on Blog Page', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_blog_page_section',
		'settings'  => 'reendex_theme_options[reendex_blog_excerpt_length]',
	) );

	/**
	* Video Page Settings.
	*/
	$wp_customize->add_section( 'reendex_video_section', array(
		'priority'          => $priority ++,
		'capability'        => 'edit_theme_options',
		'theme_supports'    => '',
		'title'             => esc_html__( 'Video Page Options', 'reendex' ),
		'description'       => '',
		'panel'             => 'reendex_options_panel',
	) );

	// Video Page size options.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_video-options]', array(
		'capability'		=> 'edit_theme_options',
		'default'           => $defaults['reendex_video-options'],
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_video-options]', array(
		'choices'		=> array(
			'fullwidth' => esc_html__( 'Full Width','reendex' ),
			'normal'    => esc_html__( 'Normal','reendex' ),
		),
		'label'		=> esc_html__( 'Select Video Width Options', 'reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_theme_options[reendex_video-options]',
		'type'	  	=> 'select',
	) );

	// Related Videos Heading.
	$wp_customize->add_setting( 'reendex_search_related_videos_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_search_related_videos_heading', array(
		'label'     => esc_html__( 'Related Videos options', 'reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_search_related_videos_heading',
	) ) );

	// Related Videos.
	$wp_customize->add_setting( 'reendex_related_video_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_related_video_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Related Videos on Video Page template.','reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_related_video_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_related_videos', array(
		'default'           => 'categories',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_related_posts_sanitize',
	) );
	$wp_customize->add_control( 'reendex_related_videos', array(
		'type'      => 'radio',
		'label'     => esc_html__( 'Related Videos Must Be Shown As:', 'reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_related_videos',
		'choices'   => array(
			'categories'    => esc_html__( 'Related Videos By Categories', 'reendex' ),
			'tags'          => esc_html__( 'Related Videos By Tags', 'reendex' ),
		),
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_video_related_number]', array(
		'default'           => $defaults['reendex_video_related_number'],
		'type'              => 'theme_mod',
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_video_related_number]', array(
		'label'         => esc_html__( 'Number of related videos', 'reendex' ),
		'section'       => 'reendex_video_section',
		'type'          => 'number',
		'description'   => 'Please save and refresh to see the changes',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_video_content_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_related_video_content_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_video_content_length]', array(
		'label'     => esc_html__( 'Related Videos Content Length', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_theme_options[reendex_related_video_content_length]',
	) );
	$wp_customize->add_setting( 'reendex_related_video_title_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_related_video_title_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Title of Related Videos.','reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_related_video_title_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_video_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_related_video_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_video_title]', array(
		'label'     => esc_html__( 'Related Videos Title','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_theme_options[reendex_related_video_title]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_related_video_subtitle]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_related_video_subtitle'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_related_video_subtitle]', array(
		'label'     => esc_html__( 'Related Videos Subtitle','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_theme_options[reendex_related_video_subtitle]',
	) );
	$wp_customize->add_setting( 'reendex_related_video_title_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'default'           => '#d4000e',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'reendex_related_video_title_bg_color', array(
		'label'         => esc_html__( 'Related Videos Title background color', 'reendex' ),
		'description'   => '',
		'type'          => 'color',
		'section'       => 'reendex_video_section',
	) ) );

	// Video Gallery Heading.
	$wp_customize->add_setting( 'reendex_search_video_gallery_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_search_video_gallery_heading', array(
		'label'     => esc_html__( 'Video Gallery options', 'reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_search_video_gallery_heading',
	) ) );

	// Video Gallery setting.
	$wp_customize->add_setting( 'reendex_gallery_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_gallery_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Video gallery on Video Page template.','reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_gallery_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_gallery_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_gallery_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_gallery_title]', array(
		'label'     => esc_html__( 'Video Gallery Title','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_theme_options[reendex_gallery_title]',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_video_categories]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_video_categories'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_setting( 'reendex_video_page_gallery_title_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#212126',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'reendex_video_page_gallery_title_bg_color', array(
		'label'         => esc_html__( 'Video Gallery Title background color', 'reendex' ),
		'description'   => '',
		'type'          => 'color',
		'section'       => 'reendex_video_section',
	) ) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_video_categories]', array(
		'label'         => esc_html__( 'Video Gallery Categories', 'reendex' ),
		'description'   => esc_html__( 'Filter video categories by ID. Enter here the video category IDs separated by commas (ex: 762).','reendex' ),
		'type'          => 'text',
		'section'       => 'reendex_video_section',
		'settings'      => 'reendex_theme_options[reendex_video_categories]',
	) );

	// Video Page Ad Banner Heading.
	$wp_customize->add_setting( 'reendex_search_ad_banner_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_search_ad_banner_heading', array(
		'label'     => esc_html__( 'Video Page Ad Banner options', 'reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_search_ad_banner_heading',
	) ) );

	// Video Page Ad Banner.
	$wp_customize->add_setting( 'video_ad_image', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'video_ad_image', array(
		'label'         => esc_html__( 'Video Page Ad Banner', 'reendex' ),
		'type'          => 'image',
		'section'       => 'reendex_video_section',
		'description'   => esc_html__( 'Recommended size: 1248 x 100px', 'reendex' ),
	) ) );
	$wp_customize->add_setting( 'video_ad_url', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'video_ad_url', array(
		'label' 	=> esc_html__( 'Video Page ad link', 'reendex' ),
		'section'   => 'reendex_video_section',
		'type' 		=> 'url',
	) );

	// Video Info Heading.
	$wp_customize->add_setting( 'reendex_search_info_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_search_info_heading', array(
		'label'     => esc_html__( 'Video Info options', 'reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_search_info_heading',
	) ) );

	// Video Info.
	$wp_customize->add_setting( 'reendex_show_video_info',array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_show_video_info', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Video Info.','reendex' ),
		'section' 	=> 'reendex_video_section',
		'settings'  => 'reendex_show_video_info',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	// Video Author.
	$wp_customize->add_setting( 'reendex_show_video_author',array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_show_video_author', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Video Author.','reendex' ),
		'section' 	=> 'reendex_video_section',
		'settings'  => 'reendex_show_video_author',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	// Video Date.
	$wp_customize->add_setting( 'reendex_show_video_date',array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_show_video_date', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Video Date.','reendex' ),
		'section' 	=> 'reendex_video_section',
		'settings'  => 'reendex_show_video_date',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	// Video Skills.
	$wp_customize->add_setting( 'reendex_show_video_skills',array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_show_video_skills', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Video Skills.','reendex' ),
		'section' 	=> 'reendex_video_section',
		'settings'  => 'reendex_show_video_skills',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	// Video Client.
	$wp_customize->add_setting( 'reendex_show_video_client',array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_show_video_client', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Video Client.','reendex' ),
		'section'   => 'reendex_video_section',
		'settings'  => 'reendex_show_video_client',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	/**
	 * Search Page Settings
	 */
	$wp_customize->add_section( 'reendex_search_page_layout_setting', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Search Page Options', 'reendex' ),
		'panel'     => 'reendex_options_panel',
	) );

	$wp_customize->add_setting( 'reendex_search_page_layout', array(
		'default'           => 'rightsidebar',
		'capability'        => 'edit_theme_options',
		'type'              => 'theme_mod',
		'transport'         => 'refresh',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Radio_Image( $wp_customize, 'reendex_search_page_layout', array(
		'label'     => esc_html__( 'Select Search Results Page Layout', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_search_page_layout',
		'choices'   => array(
			'leftsidebar'   => trailingslashit( get_template_directory_uri() ) . '/img/left-sidebar.png',
			'rightsidebar'  => trailingslashit( get_template_directory_uri() ) . '/img/right-sidebar.png',
			'nosidebar'     => trailingslashit( get_template_directory_uri() ) . '/img/no-sidebar.png',
		),
	) ) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_search_banner_show_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_search_banner_show_heading', array(
		'label'     => esc_html__( 'Header Banner Options', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_search_banner_show_heading',
	) ) );

	// Banner Section.
	$wp_customize->add_setting( 'reendex_search_banner_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_search_banner_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Header Banner on Search Results Pages.', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_search_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_search_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'       => 'reendex_search_page_layout_setting',
	) ) );

	$wp_customize->add_setting( 'reendex_search_banner_title', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_search_banner_title', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Header Banner Title and Breadcrumbs on Search Results Pages.', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Search Results Page Options Heading.
	$wp_customize->add_setting( 'reendex_search_options_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_search_options_heading', array(
		'label'     => esc_html__( 'Search Results Page Options', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_search_options_heading',
	) ) );

	// Search Results Page content styles.
	$wp_customize->add_setting( 'reendex_search_content_style', array(
		'default'           => '4',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_search_content_style', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select Search Results Page Content Style', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'choices'   => array(
			'1'  => esc_html__( 'Style 1', 'reendex' ),
			'2'  => esc_html__( 'Style 2', 'reendex' ),
			'3'  => esc_html__( 'Style 3', 'reendex' ),
			'4'  => esc_html__( 'Style 4', 'reendex' ),
		),
	) );

	// Post Read More Text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_search_readmore_text]', array(
		'default'           => $defaults['reendex_search_readmore_text'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_search_readmore_text]', array(
		'label'       	=> esc_html__( 'Read More', 'reendex' ),
		'description'   => esc_html__( 'Change the "Read More" text here.', 'reendex' ),
		'section'       => 'reendex_search_page_layout_setting',
		'settings'    	=> 'reendex_theme_options[reendex_search_readmore_text]',
		'type'         	=> 'text',
	) );

	// Disable Post Meta.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_search_disable_postmeta]', array(
		'default'           => $defaults['reendex_search_disable_postmeta'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_search_disable_postmeta]', array(
		'label' 	=> esc_html__( 'Enable Post Meta', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_theme_options[reendex_search_disable_postmeta]',
		'type' 		=> 'checkbox',
	) );

	// Disable Post Footer.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_search_disable_post_footer]', array(
		'default'           => $defaults['reendex_search_disable_post_footer'],
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'reendex_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'reendex_theme_options[reendex_search_disable_post_footer]', array(
		'label' 	=> esc_html__( 'Enable Post Footer', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_theme_options[reendex_search_disable_post_footer]',
		'type' 	    => 'checkbox',
	) );

	// Breaking News Settings.
	$wp_customize->add_setting( 'reendex_search_breaking_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_search_breaking_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Breaking News Ticker on Search Results Page.', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Featured images on search results pages.
	$wp_customize->add_setting( 'reendex_image_search_archives', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );

	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_image_search_archives', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Featured image on Search Results Page.','reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_image_search_archives',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Show Category label.
	$wp_customize->add_setting( 'reendex_search_show_category', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_search_show_category', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Category label on image.', 'reendex' ),
		'section'   => 'reendex_search_page_layout_setting',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Custom excerpt length on search results pages.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_search_excerpt_length]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_search_excerpt_length'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_search_excerpt_length]', array(
		'label'     => esc_html__( 'Excerpt character count on Search Results Page', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_search_page_layout_setting',
		'settings'  => 'reendex_theme_options[reendex_search_excerpt_length]',
	) );

	/**
	* Weather section
	*/
	$wp_customize->add_section( 'reendex_weather_section', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Weather', 'reendex' ),
		'description'   => 'Weather options',
		'panel'         => 'reendex_options_panel',
	) );
	// Weather Access Key.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_weather_access_key]', array(
		'default'           => $defaults['reendex_weather_access_key'],
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_weather_access_key]', array(
		'label'          => esc_html__( 'Weather Access Key', 'reendex' ),
		'type'           => 'text',
		'section'        => 'reendex_weather_section',
		'description'    => esc_html__( 'Please go to https://ipstack.com/ to generate your Access Key','reendex' ),
	) );
	// Weather API Key.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_wea_api]', array(
		'default'           => $defaults['reendex_wea_api'],
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_wea_api]', array(
		'label'          => esc_html__( 'Weather API Key', 'reendex' ),
		'type'           => 'text',
		'section'        => 'reendex_weather_section',
		'description'    => esc_html__( 'Please go to http://openweathermap.org/ to generate your API Key','reendex' ),
	) );
	// Weather Location.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_weather_location]', array(
		'default'           => $defaults['reendex_weather_location'],
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_weather_location]', array(
		'label'         => esc_html__( 'Weather Location', 'reendex' ),
		'type'          => 'text',
		'section'       => 'reendex_weather_section',
		'description'   => esc_html__( 'Add your location here.','reendex' ),
	) );

	/**
	 * Breaking News Settings
	 */
	$wp_customize->add_section( 'reendex_breaking_news_setting', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Breaking News Ticker Options for Single Post, Video Page, Archive Page and Search Page', 'reendex' ),
		'panel'     => 'reendex_options_panel',
	) );
	// News Ticker setting.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_single_ticker_content]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_single_ticker_content'],
		'sanitize_callback' => 'reendex_sanitize_number',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_single_ticker_content]', array(
		'label'     => esc_html__( 'News Ticker Content Length', 'reendex' ),
		'type'      => 'number',
		'section'   => 'reendex_breaking_news_setting',
		'settings'  => 'reendex_theme_options[reendex_single_ticker_content]',
	) );

	/**
	* Contact section
	*/
	$wp_customize->add_section( 'reendex_contact_section', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Contact Page', 'reendex' ),
		'description'   => 'Contact options',
	) );

	// Header Banner Heading.
	$wp_customize->add_setting( 'reendex_contact_banner_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_contact_banner_heading', array(
		'label'     => esc_html__( 'Header Banner options', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_contact_banner_heading',
	) ) );

	// Contact Page Banner Section.
	$wp_customize->add_setting( 'reendex_contact_banner_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_contact_banner_show', array(
		'type' 		 => 'switch',
		'label'      => esc_html__( 'Choose whether to display the  Header Banner on Contact Page.', 'reendex' ),
		'section'    => 'reendex_contact_section',
		'choices'    => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_contact_banner_image', array(
		'type'              => 'option',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_contact_banner_image', array(
		'label'         => esc_html__( 'Header Banner Image', 'reendex' ),
		'description'   => esc_html__( 'Required minimum height is 800px and minimum width is 1920px.', 'reendex' ),
		'section'       => 'reendex_contact_section',
	) ) );

	$wp_customize->add_setting( 'reendex_contact_banner_title_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_contact_banner_title_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Title and Subtitle on Header Banner.', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	$wp_customize->add_setting( 'reendex_contact_banner_title_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_contact_banner_title_text_color', array(
		'label'     => esc_html__( 'Title text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_contact_section',
	) );

	$wp_customize->add_setting( 'reendex_contact_banner_subtitle_bg_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#000',
	) );
	$wp_customize->add_control( 'reendex_contact_banner_subtitle_bg_color', array(
		'label'     => esc_html__( 'Subtitle background color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_contact_section',
	) );
	$wp_customize->add_setting( 'reendex_contact_banner_subtitle_text_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_contact_banner_subtitle_text_color', array(
		'label'     => esc_html__( 'Subtitle text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_contact_section',
	) );

	// Contact Page Banner Title.
	$wp_customize->add_setting( 'reendex_contact_banner_title', array(
		'type'              => 'option',
		'default'           => esc_html__( 'Contact Us', 'reendex' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_contact_banner_title', array(
		'label'     => esc_html__( 'Banner Title', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_contact_section',
	) );
	// Contact Page Banner Subtitle.
	$wp_customize->add_setting( 'reendex_contact_banner_subtitle', array(
		'type'              => 'option',
		'default'           => esc_html__( 'Look for more news from our own sources', 'reendex' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_contact_banner_subtitle', array(
		'label'     => esc_html__( 'Banner Subtitle', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_contact_section',
	) );
	// Subtitle button link.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_contact_banner_subtitle_link]', array(
		'sanitize_callback'  => 'esc_url_raw',
		'capability'         => 'edit_theme_options',
		'default'            => $defaults['reendex_contact_banner_subtitle_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact_banner_subtitle_link]', array(
		'label'     => esc_html__( 'Subtitle button link', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'type'      => 'text',
	) );

	// Contact Info Heading.
	$wp_customize->add_setting( 'reendex_contact_info_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_contact_info_heading', array(
		'label'     => esc_html__( 'Contact Info options', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_contact_info_heading',
	) ) );

	// About Us title.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_contact_page_ab_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_contact_page_ab_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact_page_ab_title]', array(
		'label'     => esc_html__( 'About Us Title','reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_theme_options[reendex_contact_page_ab_title]',
		'type'      => 'text',
	) );

	// About Us text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_contact_aboutus_text]', array(
		'sanitize_callback' => 'reendex_sanitize_textarea',
		'default'           => $defaults['reendex_contact_aboutus_text'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact_aboutus_text]', array(
		'label'     => esc_html__( 'About Us Text', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'type'      => 'textarea',
	) );

	// Contact info title.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_contact_page_info_title]', array(
		'type'               => 'theme_mod',
		'default'            => $defaults['reendex_contact_page_info_title'],
		'sanitize_callback'  => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact_page_info_title]', array(
		'label'     => esc_html__( 'Contact Info Title','reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_theme_options[reendex_contact_page_info_title]',
		'type'      => 'text',
	) );

	// Address.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_our_address]', array(
		'default'            => $defaults['reendex_our_address'],
		'sanitize_callback'	 => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_our_address]', array(
		'label'	    => esc_html__( 'Add Address here.','reendex' ),
		'section'	=> 'reendex_contact_section',
		'setting'   => 'reendex_theme_options[reendex_our_address]',
		'type'		=> 'text',
	) );

	// Phone number.
	$wp_customize->add_setting('reendex_theme_options[reendex_contact]', array(
		'default'            => $defaults['reendex_contact'],
		'sanitize_callback'	 => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact]', array(
		'label'	    => esc_html__( 'Add Phone Number here.','reendex' ),
		'section'	=> 'reendex_contact_section',
		'setting'   => 'reendex_theme_options[reendex_contact]',
		'type'		=> 'text',
	) );

	// Email.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_email]', array(
		'default'           => $defaults['reendex_email'],
		'sanitize_callback'	=> 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_email]', array(
		'label'	    => esc_html__( 'Add Email address here.','reendex' ),
		'section'	=> 'reendex_contact_section',
		'setting'   => 'reendex_theme_options[reendex_email]',
		'type'		=> 'text',
	) );

	// Google Map Options Heading.
	$wp_customize->add_setting( 'reendex_contact_map_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_contact_map_heading', array(
		'label'     => esc_html__( 'Google Map options', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_contact_map_heading',
	) ) );

	// Google Map Display Option.
	$wp_customize->add_setting( 'reendex_gmap_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_gmap_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Google Map on Contact Page.', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_gmap_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Map title.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_contact_map_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_contact_map_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact_map_title]', array(
		'label'         => esc_html__( 'Map Title','reendex' ),
		'description'   => esc_html__( 'Available for Contact Page Style 1','reendex' ),
		'section'       => 'reendex_contact_section',
		'settings'      => 'reendex_theme_options[reendex_contact_map_title]',
		'type'          => 'text',
	) );
	$wp_customize->add_setting( 'reendex_contact_map_title_show', array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_contact_map_title_show', array(
		'type' 		    => 'switch',
		'label'         => esc_html__( 'Choose whether to display the Title of Google Map.','reendex' ),
		'description'   => esc_html__( 'This applies to Contact Page Style 1','reendex' ),
		'section'       => 'reendex_contact_section',
		'settings'      => 'reendex_contact_map_title_show',
		'choices'       => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );

	// Google Maps API Key.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_google_maps_api_key]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_google_maps_api_key'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_google_maps_api_key]', array(
		'label'     => esc_html__( 'Google Maps API Key', 'reendex' ),
		'type'      => 'text',
		'settings'  => 'reendex_theme_options[reendex_google_maps_api_key]',
		'section'   => 'reendex_contact_section',
	) );
	// Google Map Address.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_google_address]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_google_address'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_google_address]', array(
		'label'         => esc_html__( 'Google Map Address','reendex' ),
		'description'   => esc_html__( 'Please provide the google map address of your location.','reendex' ),
		'type'          => 'textarea',
		'section'       => 'reendex_contact_section',
		'settings'      => 'reendex_theme_options[reendex_google_address]',
	) );

	// Google Map Zoom.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_gmap_zoom]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_gmap_zoom'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_gmap_zoom]', array(
		'label'     => esc_html__( 'Google Map Zoom','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_theme_options[reendex_gmap_zoom]',
	) );

	// Contact Form Heading.
	$wp_customize->add_setting( 'reendex_contact_form_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_contact_form_heading', array(
		'label'     => esc_html__( 'Contact Form options', 'reendex' ),
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_contact_form_heading',
	) ) );

	// Contact Form.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_contact_form_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_contact_form_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_contact_form_title]', array(
		'label'     => esc_html__( 'Contact Form Title','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_contact_section',
		'settings'  => 'reendex_theme_options[reendex_contact_form_title]',
	) );
	$wp_customize->add_setting( 'reendex_contact_form', array(
		'type'              => 'option',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_contact_form', array(
		'label'         => esc_html__( 'Contact Form', 'reendex' ),
		'description'   => esc_html__( 'Use contact form 7 shortcode.', 'reendex' ),
		'section'       => 'reendex_contact_section',
		'type'          => 'text',
	) );

	/**
	* Coming Soon Page
	*/
	$wp_customize->add_section( 'reendex_comingsoon', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Coming Soon Page', 'reendex' ),
	) );

	$wp_customize->add_setting( 'reendex_theme_options[reendex_comingsoon_date]', array(
		'default'            => $defaults['reendex_comingsoon_date'],
		'type'               => 'theme_mod',
		'capability'         => 'edit_theme_options',
		'transport'          => '',
		'sanitize_callback'  => 'reendex_sanitize_date',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_comingsoon_date]', array(
		'type'          => 'date',
		'section'       => 'reendex_comingsoon',
		'label'         => esc_html__( 'Date Field', 'reendex' ),
		'description'   => '',
	) );
	$wp_customize->add_setting( 'reendex_comingsoon_show', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_comingsoon_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Do you want to activate the Coming soon page?','reendex' ),
		'section'   => 'reendex_comingsoon',
		'settings'  => 'reendex_comingsoon_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Yes', 'reendex' ),
			'disable'  => esc_html__( 'No', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_newsletter', array(
		'type'              => 'option',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_newsletter', array(
		'label'         => esc_html__( 'Newsletter', 'reendex' ),
		'description'   => esc_html__( 'Use Newsletter shortcode here.', 'reendex' ),
		'section'       => 'reendex_comingsoon',
		'type'          => 'text',
	) );
	$wp_customize->add_setting( 'reendex_comingsoon_socialmedia_show',array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_comingsoon_socialmedia_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Social Media on Coming soon page.', 'reendex' ),
		'section'   => 'reendex_comingsoon',
		'settings'  => 'reendex_comingsoon_socialmedia_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_comingsoon_socialmedia_title_show',array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_comingsoon_socialmedia_title_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display the Title of Social Media.','reendex' ),
		'section'   => 'reendex_comingsoon',
		'settings'  => 'reendex_comingsoon_socialmedia_title_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_comingsoon_socialmedia_title]', array(
		'type'              => 'theme_mod',
		'default'           => $defaults['reendex_comingsoon_socialmedia_title'],
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_comingsoon_socialmedia_title]', array(
		'label'     => esc_html__( 'Social Media Title','reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_comingsoon',
		'settings'  => 'reendex_theme_options[reendex_comingsoon_socialmedia_title]',
	) );

	/**
	* Footer section
	*/
	$wp_customize->add_section( 'reendex_footer', array(
		'priority'          => $priority ++,
		'capability'        => 'edit_theme_options',
		'theme_supports'    => '',
		'title'             => esc_html__( 'Footer Options', 'reendex' ),
		'description'       => '',
	) );
	$wp_customize->add_setting( 'reendex_footer_background_image', array(
		'default'           => '',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'reendex_footer_background_image', array(
		'label'     => esc_html__( 'Upload Footer Background Image', 'reendex' ),
		'section'   => 'reendex_footer',
		'setting'   => 'reendex_footer_background_image',
	) ) );

	// Copyright text.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_copyright_text]',array(
		'type'              => 'theme_mod',
		'default'           => '',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'reendex_sanitize_allowed_tags',
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_copyright_text]',array(
		'label'         => esc_html__( 'Footer Copyright Text', 'reendex' ),
		'description'   => esc_html__( 'Enter Footer Copyright Text here. Allowed tags : a, b, strong, em.', 'reendex' ),
		'type'          => 'textarea',
		'section'       => 'reendex_footer',
		'settings'      => 'reendex_theme_options[reendex_copyright_text]',
	) );
	$wp_customize->add_setting( 'reendex_copyright_socialmedia_show',array(
		'default'            => 'enable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_copyright_socialmedia_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Social Media on Footer.','reendex' ),
		'section'   => 'reendex_footer',
		'settings'  => 'reendex_copyright_socialmedia_show',
		'choices'   => array(
			'enable'   => esc_html__( 'Show', 'reendex' ),
			'disable'  => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex-footer_widgets', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex-footer_widgets', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Footer Widget Area', 'reendex' ),
		'section'   => 'reendex_footer',
		'settings'  => 'reendex-footer_widgets',
		'choices'   => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
	) ) );

	// Social Media section.
	$wp_customize->add_section( 'reendex_socialmedia_section', array(
		'priority'      => $priority ++,
		'capability'    => 'edit_theme_options',
		'title'         => esc_html__( 'Social Media', 'reendex' ),
		'description'   => esc_html__( 'Social Media Icons will be displayed on Header / Footer / Coming Soon Page', 'reendex' ),
	) );
	// Setting - Social Media.
	$wp_customize->add_setting( 'reendex_theme_options[reendex_facebook_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_facebook_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_facebook_link]', array(
		'label'     => esc_html__( 'Facebook Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_twitter_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_twitter_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_twitter_link]', array(
		'label'     => esc_html__( 'Twitter Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_pinterest_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_pinterest_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_pinterest_link]', array(
		'label'     => esc_html__( 'Pinterest Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_googleplus_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_googleplus_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_googleplus_link]', array(
		'label'     => esc_html__( 'Google Plus Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_vimeo_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_vimeo_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_vimeo_link]', array(
		'label'     => esc_html__( 'Vimeo Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_youtube_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_youtube_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_youtube_link]', array(
		'label'     => esc_html__( 'Youtube Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_linkedin_link]', array(
		'sanitize_callback' => 'esc_url_raw',
		'capability'        => 'edit_theme_options',
		'default'           => $defaults['reendex_linkedin_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_linkedin_link]', array(
		'label'     => esc_html__( 'Linkedin Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_rss_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_rss_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_rss_link]', array(
		'label'     => esc_html__( 'Rss Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_tumblr_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_tumblr_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_tumblr_link]', array(
		'label'     => esc_html__( 'Tumblr Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_reddit_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_reddit_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_reddit_link]', array(
		'label'     => esc_html__( 'Reddit Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_stumbleupon_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_stumbleupon_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_stumbleupon_link]', array(
		'label'     => esc_html__( 'Stumbleupon Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_digg_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_digg_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_digg_link]', array(
		'label'     => esc_html__( 'Digg Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );
	$wp_customize->add_setting( 'reendex_theme_options[reendex_instagram_link]', array(
		'sanitize_callback'   => 'esc_url_raw',
		'capability'          => 'edit_theme_options',
		'default'             => $defaults['reendex_instagram_link'],
	) );
	$wp_customize->add_control( 'reendex_theme_options[reendex_instagram_link]', array(
		'label'     => esc_html__( 'Instagram Link', 'reendex' ),
		'section'   => 'reendex_socialmedia_section',
		'type'      => 'text',
	) );

	// Header Ad section.
	$wp_customize->add_section( 'reendex_header_ad', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Header Advertising', 'reendex' ),
		'panel'		=> 'reendex_advertising_panel',
	) );

	// Ad Types.
	$wp_customize->add_setting( 'reendex_header_ad_type', array(
		'default'           => 'image',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_header_ad_type', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select the type of Ad you want to use.', 'reendex' ),
		'section'   => 'reendex_header_ad',
		'choices'   => array(
			'image'      => esc_html__( 'Image Ad', 'reendex' ),
			'code'       => esc_html__( 'Script Code', 'reendex' ),
			'googleads'  => esc_html__( 'Google Ad', 'reendex' ),
			'shortcode'  => esc_html__( 'Shortcode', 'reendex' ),
		),
	) );

	// Image Ad.
	$wp_customize->add_setting( 'reendex_header_image_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_header_image_ad_heading', array(
		'label'     => esc_html__( 'Image Ad', 'reendex' ),
		'section'   => 'reendex_header_ad',
		'settings'  => 'reendex_header_image_ad_heading',
		'active_callback' => 'reendex_header_ad_type_image',
	) ) );
	$wp_customize->add_setting( 'reendex_header_ad_image', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_header_ad_image', array(
		'label'         => esc_html__( 'Header ad image', 'reendex' ),
		'type'          => 'image',
		'section'       => 'reendex_header_ad',
		'description'   => esc_html__( 'Recommended size: 728 x 90px', 'reendex' ),
		'active_callback' => 'reendex_header_ad_type_image',
	) ) );
	$wp_customize->add_setting( 'reendex_header_ad_url', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'reendex_header_ad_url', array(
		'label' 	=> esc_html__( 'Header ad link', 'reendex' ),
		'section' 	=> 'reendex_header_ad',
		'type'      => 'url',
		'priority'  => 10,
		'active_callback' => 'reendex_header_ad_type_image',
	) );
	$wp_customize->add_setting( 'reendex_header_ad_target', array(
		'sanitize_callback'   => 'sanitize_text_field',
		'capability'          => 'edit_theme_options',
		'default'             => '_blank',
	) );
	$wp_customize->add_control( 'reendex_header_ad_target', array(
		'label'         => esc_html__( 'Link Target Options', 'reendex' ),
		'description'   => esc_html__( 'Here you can specify how to open the Ad Image link.', 'reendex' ),
		'choices'       => array(
			'_self'     => esc_html__( 'Open the link in the same browser window', 'reendex' ),
			'_blank'    => esc_html__( 'Open the link in a new browser window', 'reendex' ),
		),
		'type'      => 'select',
		'section'   => 'reendex_header_ad',
		'active_callback' => 'reendex_header_ad_type_image',
	) );

	// Script Code.
	$wp_customize->add_setting( 'reendex_header_script_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_header_script_ad_heading', array(
		'label'     => esc_html__( 'Script Code', 'reendex' ),
		'section'   => 'reendex_header_ad',
		'settings'  => 'reendex_header_script_ad_heading',
		'active_callback' => 'reendex_header_ad_type_code',
	) ) );
	$wp_customize->add_setting( 'reendex_header_script_ad', array(
		'sanitize_callback' => 'reendex_sanitize_banner',
	) );
	$wp_customize->add_control( 'reendex_header_script_ad', array(
		'label'         => esc_html__( 'Script Code', 'reendex' ),
		'type'          => 'textarea',
		'section'       => 'reendex_header_ad',
		'description'   => esc_html__( 'Paste your ad code here from Google AdSense or any other provider.', 'reendex' ),
		'active_callback' => 'reendex_header_ad_type_code',
	) );

	// Google Ad.
	$wp_customize->add_setting( 'reendex_header_google_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_header_google_ad_heading', array(
		'label'     => esc_html__( 'Google Ad', 'reendex' ),
		'section'   => 'reendex_header_ad',
		'settings'  => 'reendex_header_google_ad_heading',
		'active_callback' => 'reendex_header_ad_type_google',
	) ) );
	$wp_customize->add_setting( 'reendex_header_ad_publisher_id', array(
		'default'            => '',
		'capability'         => 'edit_theme_options',
		'sanitize_callback'  => 'sanitize_text_field',
	));
	$wp_customize->add_control( 'reendex_header_ad_publisher_id', array(
		'label'         => esc_html__( 'Google Ad Publisher ID', 'reendex' ),
		'description'   => esc_html__( 'Insert data-ad-client / google_ad_client content.', 'reendex' ),
		'section'       => 'reendex_header_ad',
		'type'          => 'text',
		'active_callback' => 'reendex_header_ad_type_google',
	) );
	$wp_customize->add_setting( 'reendex_header_ad_slot_id', array(
		'default'            => '',
		'capability'         => 'edit_theme_options',
		'sanitize_callback'  => 'sanitize_text_field',
	));
	$wp_customize->add_control( 'reendex_header_ad_slot_id', array(
		'label'             => esc_html__( 'Google Ad Slot ID', 'reendex' ),
		'description'       => esc_html__( 'Insert data-ad-slot / google_ad_slot content.', 'reendex' ),
		'section'           => 'reendex_header_ad',
		'type'              => 'text',
		'active_callback'   => 'reendex_header_ad_type_google',
	) );
	$wp_customize->add_setting( 'reendex_header_ad_desktop_size', array(
		'default'           => 'auto',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_header_ad_desktop_size', array(
		'type'	  	        => 'select',
		'label'             => esc_html__( 'Choose the desktop ad size (auto is recommended).', 'reendex' ),
		'section'           => 'reendex_header_ad',
		'choices'           => $get_ad_size,
		'active_callback'   => 'reendex_header_ad_type_google',
	) );

	// Shortcode.
	$wp_customize->add_setting( 'reendex_header_shortcode_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_header_shortcode_ad_heading', array(
		'label'     => esc_html__( 'Shortcode', 'reendex' ),
		'section'   => 'reendex_header_ad',
		'settings'  => 'reendex_header_shortcode_ad_heading',
		'active_callback' => 'reendex_header_ad_type_shortocde',
	) ) );
	$wp_customize->add_setting( 'reendex_header_ad_shortcode', array(
		'sanitize_callback' => 'reendex_sanitize_banner',
	) );
	$wp_customize->add_control( 'reendex_header_ad_shortcode', array(
		'label'         => esc_html__( 'Advertisement Shortcode', 'reendex' ),
		'type'          => 'textarea',
		'section'       => 'reendex_header_ad',
		'description'   => esc_html__( 'Add your ad\'s shortcode here', 'reendex' ),
		'active_callback' => 'reendex_header_ad_type_shortocde',
	) );
	// Header Ad Bottom Text.
	$wp_customize->add_setting( 'header_ad_bottom_text', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default'           => '',
	) );
	$wp_customize->add_control( 'header_ad_bottom_text', array(
		'label'     => esc_html__( 'Header Ad Bottom Text', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_header_ad',
	) );

	// Above Header Ad section.
	$wp_customize->add_section( 'reendex_above_header_ad', array(
		'priority'  => $priority ++,
		'title'     => esc_html__( 'Above Header Advertising', 'reendex' ),
		'panel'		=> 'reendex_advertising_panel',
	) );
	// Enable Above Header Advertising.
	$wp_customize->add_setting( 'reendex_above_header_ad_enable', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_above_header_ad_enable', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Enable Above Header Advertising', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'settings'  => 'reendex_above_header_ad_enable',
		'choices'   => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
	) ) );
	// Ad Types.
	$wp_customize->add_setting( 'reendex_above_header_ad_type', array(
		'default'           => 'image',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_type', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Select the type of Ad you want to use.', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'choices'   => array(
			'image'  => esc_html__( 'Image Ad', 'reendex' ),
			'code'  => esc_html__( 'Script Code', 'reendex' ),
			'googleads'  => esc_html__( 'Google Ad', 'reendex' ),
			'shortcode'  => esc_html__( 'Shortcode', 'reendex' ),
		),
	) );

	// Image Ad.
	$wp_customize->add_setting( 'reendex_above_header_image_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_above_header_image_ad_heading', array(
		'label'     => esc_html__( 'Image Ad', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'settings'  => 'reendex_above_header_image_ad_heading',
		'active_callback' => 'reendex_above_header_ad_type_image',
	) ) );
	$wp_customize->add_setting( 'reendex_above_header_ad_image', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reendex_above_header_ad_image', array(
		'label'         => esc_html__( 'Above Header ad image', 'reendex' ),
		'type'          => 'image',
		'section'       => 'reendex_above_header_ad',
		'description'   => esc_html__( 'Recommended size: 970 x 90px', 'reendex' ),
		'active_callback' => 'reendex_above_header_ad_type_image',
	) ) );
	$wp_customize->add_setting( 'reendex_above_header_ad_url', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_url', array(
		'label' 	=> esc_html__( 'Header ad link', 'reendex' ),
		'section' 	=> 'reendex_above_header_ad',
		'type'      => 'url',
		'priority'  => 10,
		'active_callback' => 'reendex_above_header_ad_type_image',
	) );
	$wp_customize->add_setting( 'reendex_above_header_ad_target', array(
		'sanitize_callback'   => 'sanitize_text_field',
		'capability'          => 'edit_theme_options',
		'default'             => '_blank',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_target', array(
		'label'         => esc_html__( 'Link Target Options', 'reendex' ),
		'description'   => esc_html__( 'Here you can specify how to open the Ad Image link.', 'reendex' ),
		'choices'       => array(
			'_self'     => esc_html__( 'Open the link in the same browser window', 'reendex' ),
			'_blank'    => esc_html__( 'Open the link in a new browser window', 'reendex' ),
		),
		'type'      => 'select',
		'section'   => 'reendex_above_header_ad',
		'active_callback' => 'reendex_above_header_ad_type_image',
	) );

	// Script Code.
	$wp_customize->add_setting( 'reendex_above_header_script_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_above_header_script_ad_heading', array(
		'label'     => esc_html__( 'Script Code', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'settings'  => 'reendex_above_header_script_ad_heading',
		'active_callback' => 'reendex_above_header_ad_type_code',
	) ) );
	$wp_customize->add_setting( 'reendex_above_header_script_ad', array(
		'sanitize_callback' => 'reendex_sanitize_banner',
	) );
	$wp_customize->add_control( 'reendex_above_header_script_ad', array(
		'label'         => esc_html__( 'Script Code', 'reendex' ),
		'type'          => 'textarea',
		'section'       => 'reendex_above_header_ad',
		'description'   => esc_html__( 'Paste your ad code here from Google AdSense or any other provider.', 'reendex' ),
		'active_callback' => 'reendex_above_header_ad_type_code',
	) );

	// Google Ad.
	$wp_customize->add_setting( 'reendex_above_header_google_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_above_header_google_ad_heading', array(
		'label'     => esc_html__( 'Google Ad', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'settings'  => 'reendex_above_header_google_ad_heading',
		'active_callback' => 'reendex_above_header_ad_type_google',
	) ) );
	$wp_customize->add_setting( 'reendex_above_header_ad_publisher_id', array(
		'default'            => '',
		'capability'         => 'edit_theme_options',
		'sanitize_callback'  => 'sanitize_text_field',
	));
	$wp_customize->add_control( 'reendex_above_header_ad_publisher_id', array(
		'label'         => esc_html__( 'Google Ad Publisher ID', 'reendex' ),
		'description'   => esc_html__( 'Insert data-ad-client / google_ad_client content.', 'reendex' ),
		'section'       => 'reendex_above_header_ad',
		'type'          => 'text',
		'active_callback' => 'reendex_above_header_ad_type_google',
	) );
	$wp_customize->add_setting( 'reendex_above_header_ad_slot_id', array(
		'default'            => '',
		'capability'         => 'edit_theme_options',
		'sanitize_callback'  => 'sanitize_text_field',
	));
	$wp_customize->add_control( 'reendex_above_header_ad_slot_id', array(
		'label'         => esc_html__( 'Google Ad Slot ID', 'reendex' ),
		'description'   => esc_html__( 'Insert data-ad-slot / google_ad_slot content.', 'reendex' ),
		'section'       => 'reendex_above_header_ad',
		'type'          => 'text',
		'active_callback' => 'reendex_above_header_ad_type_google',
	) );
	$wp_customize->add_setting( 'reendex_above_header_ad_desktop_size', array(
		'default'           => 'auto',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_desktop_size', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Choose the desktop ad size (auto is recommended).', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'choices'   => $get_ad_size,
		'active_callback' => 'reendex_above_header_ad_type_google',
	) );
	$wp_customize->add_setting( 'reendex_above_header_ad_tablet_size', array(
		'default'           => 'auto',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_tablet_size', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Choose the tablet ad size (auto is recommended).', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'choices'   => $get_ad_size,
		'active_callback' => 'reendex_above_header_ad_type_google',
	) );
	$wp_customize->add_setting( 'reendex_above_header_ad_phone_size', array(
		'default'           => 'auto',
		'sanitize_callback' => 'reendex_sanitize_select',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_phone_size', array(
		'type'	  	=> 'select',
		'label'     => esc_html__( 'Choose the phone ad size (auto is recommended).', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'choices'   => $get_ad_size,
		'active_callback' => 'reendex_above_header_ad_type_google',
	) );

	// Shortcode.
	$wp_customize->add_setting( 'reendex_above_header_shortcode_ad_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_above_header_shortcode_ad_heading', array(
		'label'     => esc_html__( 'Shortcode', 'reendex' ),
		'section'   => 'reendex_above_header_ad',
		'settings'  => 'reendex_above_header_shortcode_ad_heading',
		'active_callback' => 'reendex_above_header_ad_type_shortocde',
	) ) );
	$wp_customize->add_setting( 'reendex_above_header_ad_shortcode', array(
		'sanitize_callback' => 'reendex_sanitize_banner',
	) );
	$wp_customize->add_control( 'reendex_above_header_ad_shortcode', array(
		'label'         => esc_html__( 'Advertisement Shortcode', 'reendex' ),
		'type'          => 'textarea',
		'section'       => 'reendex_above_header_ad',
		'description'   => esc_html__( 'Add your ad\'s shortcode here', 'reendex' ),
		'active_callback' => 'reendex_above_header_ad_type_shortocde',
	) );
	// Above Header Ad Bottom Text.
	$wp_customize->add_setting( 'above_header_ad_bottom_text', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default'           => '',
	) );
	$wp_customize->add_control( 'above_header_ad_bottom_text', array(
		'label'     => esc_html__( 'Above Header Ad Bottom Text', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_above_header_ad',
	) );

	// Responsive Menu section.
	// Responsive Menu General.
	$wp_customize->add_section( 'reendex_rmgeneral', array(
		'priority'      => $priority ++,
		'title'         => esc_html__( 'General Options', 'reendex' ),
		'panel'         => 'reendex_responsive_menu',
		'description'   => '',
	) );
	// Sticky mobile menu.
	$wp_customize->add_setting( 'reendex_sticky_mobile_menu', array(
		'default'            => 'disable',
		'sanitize_callback'  => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_sticky_mobile_menu', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Enable sticky mobile menu', 'reendex' ),
		'section'   => 'reendex_rmgeneral',
		'settings'  => 'reendex_sticky_mobile_menu',
		'choices'   => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'swipe', array(
		'default'             => 'disable',
		'sanitize_callback'   => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'swipe', array(
		'type' 		    => 'switch',
		'label'         => esc_html__( 'Enable Swipe', 'reendex' ),
		'description'   => esc_html__( 'Enable swipe gesture to open/close menus.', 'reendex' ),
		'choices'       => array(
			'enable'    => esc_html__( 'Enable', 'reendex' ),
			'disable'   => esc_html__( 'Disable', 'reendex' ),
		),
		'type'      => 'radio',
		'section'   => 'reendex_rmgeneral',
	) ) );
	$wp_customize->add_setting( 'reendex_rm_search_box', array(
		'sanitize_callback'   => 'sanitize_text_field',
		'capability'          => 'edit_theme_options',
		'default'             => 'below_menu',
	) );
	$wp_customize->add_control( 'reendex_rm_search_box', array(
		'label'         => esc_html__( 'Search Field Position', 'reendex' ),
		'description'   => esc_html__( 'Select the position of search box or simply hide the search box if you do not need it.', 'reendex' ),
		'choices'       => array(
			'above_menu'    => esc_html__( 'Above Menu', 'reendex' ),
			'below_menu'    => esc_html__( 'Below Menu', 'reendex' ),
			'hide'          => esc_html__( 'Hide search box', 'reendex' ),
		),
		'type'      => 'select',
		'section'   => 'reendex_rmgeneral',
	) );
	$wp_customize->add_setting( 'zooming', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'zooming', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Allow zoom on mobile devices', 'reendex' ),
		'section'   => 'reendex_rmgeneral',
		'choices'   => array(
			'enable'    => esc_html__( 'Yes', 'reendex' ),
			'disable'   => esc_html__( 'No', 'reendex' ),
		),
	) ) );
	$wp_customize->add_setting( 'reendex_rm_socialmedia_show', array(
		'default'           => 'enable',
		'sanitize_callback' => 'reendex_customizer_callback_sanitize_switch',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Switch( $wp_customize, 'reendex_rm_socialmedia_show', array(
		'type' 		=> 'switch',
		'label'     => esc_html__( 'Choose whether to display Social Media on Mobile Menu.', 'reendex' ),
		'section'   => 'reendex_rmgeneral',
		'settings'  => 'reendex_rm_socialmedia_show',
		'choices'   => array(
			'enable'    => esc_html__( 'Show', 'reendex' ),
			'disable'   => esc_html__( 'Hide', 'reendex' ),
		),
	) ) );
	// Responsive Menu Settings.
	$wp_customize->add_section( 'reendex_rmsettings', array(
		'priority'      => $priority ++,
		'title'         => esc_html__( 'Menu Appearance', 'reendex' ),
		'panel'         => 'reendex_responsive_menu',
		'description'   => '',
	));

	$wp_customize->add_setting( 'reendex_rm_position', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default' => 'left',
	) );
	$wp_customize->add_control( 'reendex_rm_position', array(
		'label' => esc_html__( 'Menu Open Direction', 'reendex' ),
		'description' => esc_html__( 'Select the direction from where menu will open.', 'reendex' ),
		'choices' => array(
			'left'  => esc_html__( 'Left', 'reendex' ),
			'right' => esc_html__( 'Right', 'reendex' ),
		),
		'type'     => 'select',
		'section'  => 'reendex_rmsettings',
	) );

	// Menu Top Header Heading.
	$wp_customize->add_setting( 'reendex_menu_bar_heading', array(
		'type'              => 'option',
		'capability'        => 'edit_theme_options',
		'theme_supports'    => '',
		'default'           => '',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_menu_bar_heading', array(
		'label'     => esc_html__( 'Menu Top Bar options', 'reendex' ),
		'section'   => 'reendex_rmsettings',
		'settings'  => 'reendex_menu_bar_heading',
	) ) );

	$wp_customize->add_setting( 'reendex_rm_menu_symbol_pos', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default'           => 'right',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_symbol_pos', array(
		'label'         => esc_html__( 'Menu Symbol Position', 'reendex' ),
		'description'   => esc_html__( 'Select menu icon position which will be displayed on the menu bar.', 'reendex' ),
		'choices'       => array(
			'left'   => esc_html__( 'Left', 'reendex' ),
			'right'  => esc_html__( 'Right', 'reendex' ),
		),
		'type'      => 'select',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_bar_bgd', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fcfcfc',
	) );
	$wp_customize->add_control( 'reendex_rm_bar_bgd', array(
		'label'         => esc_html__( 'Menu bar background color', 'reendex' ),
		'description'   => '',
		'type'          => 'color',
		'section'       => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_bar_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#777',
	) );
	$wp_customize->add_control( 'reendex_rm_bar_color', array(
		'label'     => esc_html__( 'Menu bar symbol color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_bar_opacity', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default'           => '0.9',
	) );
	$wp_customize->add_control( 'reendex_rm_bar_opacity', array(
		'label'     => esc_html__( 'Menu bar opacity', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_rmsettings',
	) );

	// Menu Heading.
	$wp_customize->add_setting( 'reendex_mobile_menu_heading', array(
		'type'                 => 'option',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => '',
		'default'              => '',
		'transport'            => 'refresh',
		'sanitize_callback'    => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Reendex_Customizer_Section( $wp_customize, 'reendex_mobile_menu_heading', array(
		'label'     => esc_html__( 'Menu options', 'reendex' ),
		'section'   => 'reendex_rmsettings',
		'settings'  => 'reendex_mobile_menu_heading',
	) ) );

	$wp_customize->add_setting( 'reendex_rm_menu_bgd', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#2c2c34',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_bgd', array(
		'label'     => esc_html__( 'Menu background color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_menu_opacity', array(
		'sanitize_callback' => 'sanitize_text_field',
		'capability'        => 'edit_theme_options',
		'default'           => '0.98',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_opacity', array(
		'label'     => esc_html__( 'Menu opacity', 'reendex' ),
		'type'      => 'text',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_menu_color', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#b5b5b5',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_color', array(
		'label'     => esc_html__( 'Menu text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_menu_color_hover', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#fff',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_color_hover', array(
		'label'     => esc_html__( 'Menu mouse over text color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_menu_border_top', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#55555b',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_border_top', array(
		'label'     => esc_html__( 'Menu borders (top & left) color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_menu_border_bottom', array(
		'sanitize_callback' => 'sanitize_hex_color',
		'capability'        => 'edit_theme_options',
		'default'           => '#55555b',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_border_bottom', array(
		'label'     => esc_html__( 'Menu borders (bottom) color', 'reendex' ),
		'type'      => 'color',
		'section'   => 'reendex_rmsettings',
	) );
	$wp_customize->add_setting( 'reendex_rm_menu_border_bottom_show', array(
		'sanitize_callback' => 'reendex_sanitize_select',
		'capability'        => 'edit_theme_options',
		'default'           => 'no',
	) );
	$wp_customize->add_control( 'reendex_rm_menu_border_bottom_show', array(
		'label'     => esc_html__( 'Enable bottom border for menu items', 'reendex' ),
		'choices'   => array(
			'yes'   => esc_html__( 'Yes', 'reendex' ),
			'no'    => esc_html__( 'No', 'reendex' ),
		),
		'type'      => 'radio',
		'section'   => 'reendex_rmsettings',
	) );
}
add_action( 'customize_register', 'reendex_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function reendex_customize_preview_js() {
	wp_enqueue_script( 'reendex_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'reendex_customize_preview_js' );

/**
 *  Get theme options
 *
 * @param null
 * @return array reendex_theme_options
 */
if ( ! function_exists( 'reendex_get_theme_options' ) ) :
	/**
	 * Merge values from default options array and values from customizer
	 *
	 * @return array Values returned from customizer
	 */
	function reendex_get_theme_options() {
		$reendex_default_theme_options = reendex_get_default_theme_options();
		$reendex_get_theme_options = get_theme_mod( 'reendex_theme_options' );
		if ( is_array( $reendex_get_theme_options ) ) {
			return array_merge( $reendex_default_theme_options, $reendex_get_theme_options );
		} else {
			return $reendex_default_theme_options;
		}
	}
endif;
