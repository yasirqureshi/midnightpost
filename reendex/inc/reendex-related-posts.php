<?php
/**
 * Related posts by category and tags.
 *
 * @package Reendex
 */

$related_posts = reendex_related_posts_function();
$options = reendex_get_theme_options();
$show_related_category_title = get_theme_mod( 'reendex_related_category_title_show', 'enable' );
$related_category_title = $options['reendex_related_category_title'];
$related_category_subtitle = $options['reendex_related_category_subtitle'];
$related_content_length = $options['reendex_related_content_length'];
?>

<?php if ( $related_posts->have_posts() ) : ?>
	<div class="module-wrapper related-posts">
		<?php if ( 'disable' !== $show_related_category_title ) : ?>	    
			<div class="module-title">
				<h4 class="title"><span class="bg-15"><?php echo esc_attr( $related_category_title );?></span></h4>
				<h4 class="subtitle"><?php echo esc_attr( $related_category_subtitle );?></h4>
			</div><!-- /.module-title -->
		<?php endif;?>	
		<ul class="related-category"> 
			<?php
			while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
				<li>
					<div class="item">
						<?php if ( has_post_thumbnail() ) :
							$image_id = get_post_thumbnail_id();
							$image_path = wp_get_attachment_image_src( $image_id, 'reendex_news1_thumb', true );
							$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
						?>
							<div class="item-image">
								<a class="img-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<img class="img-responsive img-full" src="<?php echo esc_url( $image_path[0] ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" title="<?php the_title(); ?>" />
									<?php if ( has_post_format( 'video' ) ) : ?>
										<span class="video-icon-small">
											<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
										</span>
									<?php endif; ?>
								</a>   
							</div><!-- /.item-image -->
						<?php endif; ?>
						<div class="item-content">
							<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
							<p><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $related_content_length ) ); ?></a></p>
						</div><!-- .item-content -->
					</div><!-- /.item -->
				</li>
			<?php endwhile; ?>
		</ul><!-- /.related-category -->
	</div><!-- /.module-wrapper -->
<?php endif; ?>
<?php wp_reset_postdata(); ?>
