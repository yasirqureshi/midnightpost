<?php
/**
 * Control - Switch.
 *
 * @package Reendex
 */

if ( class_exists( 'WP_Customize_Control' ) ) {
	/**
	 * Custom Control for Customizer Switch Button.
	 *
	 * @since Reendex 1.0
	 *
	 * @see WP_Customize_Control
	 */
	class Reendex_Customizer_Switch extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 *
		 * @since  1.0
		 * @access public
		 * @var    string
		 */
		public $type = 'switch';

		/**
		 * Render the control to be displayed in the Customizer.
		 *
		 * @since Reendex 1.0
		 */
		public function render_content() {
		?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<div class="description customize-control-description"><?php echo esc_html( $this->description ); ?></div>
				<div class="switch_button">
					<?php
					$show_choices = $this->choices;
					foreach ( $show_choices as $key => $value ) {
						echo '<span class="switch_part ' . esc_attr( $key ) . '"data-switch="' . esc_attr( $key ) . '">' . esc_attr( $value ) . '</span>';
					}
					?>
					<input type="hidden" id="switch_button" <?php $this->link(); ?> value="<?php echo esc_attr( $this->value() ); ?>" />
				</div><!-- /.switch_button -->
			</label>
		<?php
		}
	}
} // End if().
