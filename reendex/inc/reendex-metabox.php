<?php
/**
 * Theme custom metaboxes.
 *
 * @package Reendex
 */

add_action( 'cmb2_admin_init','reendex_metabox' );

/**
 * CMB2 MetaBoxes.
 *
 * @since 1.0
 */
function reendex_metabox() {
	$prefix = 'reendex_';

	// Meta Box for Page Option.
	$page_option = new_cmb2_box(array(
		'id'	        => $prefix . ' page_option ',
		'title'	        => esc_html__( 'Page Settings', 'reendex' ),
		'object_types'  => array( 'page' ), // Post type.
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left.
	));
	$page_option->add_field( array(
		'name'      => esc_html__( 'Page Primary Color', 'reendex' ),
		'id'        => $prefix . 'primary_color',
		'desc'      => esc_html__( 'Select the color you would like to use for this page. ','reendex' ),
		'type'      => 'select',
		'default'   => '1',
		'options'           => array(
			'' => esc_html__( 'None', 'reendex' ),
			'red'           => esc_html__( 'RED', 'reendex' ),
			'blue'          => esc_html__( 'BLUE', 'reendex' ),
			'dark-blue'     => esc_html__( 'DARK-BLUE', 'reendex' ),
			'green'         => esc_html__( 'GREEN', 'reendex' ),
			'purple'        => esc_html__( 'PURPLE', 'reendex' ),
			'orange'        => esc_html__( 'ORANGE', 'reendex' ),
			'deep-orange'   => esc_html__( 'DEEP-ORANGE', 'reendex' ),
			'turquoise'     => esc_html__( 'TURQUOISE', 'reendex' ),
			'pink'          => esc_html__( 'PINK', 'reendex' ),
			'slate'         => esc_html__( 'SLATE', 'reendex' ),
		),
	));

	$page_option->add_field( array(
		'name'      => esc_html__( 'Page Second Menu', 'reendex' ),
		'id'        => $prefix . 'second_menu',
		'desc'      => esc_html__( 'Select the bottom menu you would like to display on this page.','reendex' ),
		'type'      => 'select',
		'default'   => '1',
		'options'   => array(
			'' => esc_html__( 'None', 'reendex' ),
			'world'         => esc_html__( 'World', 'reendex' ),
			'news'          => esc_html__( 'News', 'reendex' ),
			'sport'         => esc_html__( 'Sport', 'reendex' ),
			'health'        => esc_html__( 'Health', 'reendex' ),
			'travel'        => esc_html__( 'Travel', 'reendex' ),
			'entertainment' => esc_html__( 'Art & Entertainment', 'reendex' ),
		),
	));

	// Second Menu.
	$menu_bottom_option = new_cmb2_box(array(
		'id'	        => $prefix . 'second_menu_option',
		'title'	        => esc_html__( 'Menu Botton Area Settings', 'reendex' ),
		'object_types'  => array( 'page' ), // Post type.
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left.
	));
	$menu_bottom_option->add_field( array(
		'name'      => esc_html__( 'Second Menu', 'reendex' ),
		'id'        => $prefix . 'page_second_menu',
		'desc'      => esc_html__( 'Here, you have the option to show or hide the bottom menu.', 'reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'  => esc_html__( 'Show', 'reendex' ),
			'hide'  => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$menu_bottom_option->add_field( array(
		'name'       => esc_html__( 'Date and time', 'reendex' ),
		'id'         => $prefix . 'page_clock',
		'desc'       => esc_html__( 'Here, you have the option to show or hide the date and time on this page.', 'reendex' ),
		'type'       => 'radio_inline',
		'options'    => array(
			'cshow'  => esc_html__( 'Show', 'reendex' ),
			'chide'  => esc_html__( 'Hide', 'reendex' ),
		),
	));

	// Breaking News.
	$breaking_news_option = new_cmb2_box(array(
		'id'	        => $prefix . 'breaking_news_option',
		'title'	        => esc_html__( 'Breaking News Settings', 'reendex' ),
		'object_types'  => array( 'post', 'our-video' ), // Post type.
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left.
	));
	$breaking_news_option->add_field( array(
		'name'      => esc_html__( 'Breaking News', 'reendex' ),
		'id'        => $prefix . 'breaking_news',
		'desc'      => esc_html__( 'Here, you have the option to show or hide the Breaking News Ticker on this page.', 'reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	// Post Meta.
	$display_featured_image = new_cmb2_box(array(
		'id'	         => $prefix . 'display_featured_image',
		'title'	         => esc_html__( 'Display Featured Image', 'reendex' ),
		'object_types'   => array( 'post' ), // Post type.
		'context'        => 'side',
		'priority'       => 'low',
		'show_names'     => true, // Show field names on the left.
	));
	$display_featured_image->add_field( array(
		'name'      => esc_html__( 'Featured Image', 'reendex' ),
		'id'        => $prefix . 'display_featured_image',
		'desc'      => esc_html__( 'Show or hide the Featured Image on this page.','reendex' ),
		'type'      => 'radio_inline',
		'default'   => 'show',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$post_subtitle = new_cmb2_box( array(
		'id'            => 'subtitle',
		'title'         => esc_html__( 'Subtitle', 'reendex' ),
		'object_types'  => array( 'post' ), // Post type.
		'context'       => 'after_title',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left.
	) );

	$post_subtitle->add_field( array(
		'name'  => esc_html__( 'Enter subtitle here', 'reendex' ),
		'id'    => 'subtitle',
		'type'  => 'text',

	) );
	$post_meta_option = new_cmb2_box(array(
		'id'	         => $prefix . 'post_meta_option',
		'title'	         => esc_html__( 'Post Meta Settings', 'reendex' ),
		'object_types'   => array( 'post' ), // Post type.
		'context'        => 'normal',
		'priority'       => 'high',
		'show_names'     => true, // Show field names on the left.
	));
	$post_meta_option->add_field( array(
		'name'      => esc_html__( 'Post Meta', 'reendex' ),
		'id'        => $prefix . 'post_meta',
		'desc'      => esc_html__( 'Here, you have the option to show or hide Post Meta on this page.','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	// Single Post Banner.
	$header_banner_option = new_cmb2_box( array(
		'id'	        => $prefix . 'single_post_banner_option',
		'title'	        => esc_html__( 'Header Banner Settings', 'reendex' ),
		'object_types'  => array( 'post' ), // Post type.
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left.
	));
	$header_banner_option->add_field( array(
		'name'      => esc_html__( 'Header Banner', 'reendex' ),
		'id'        => $prefix . 'post_banner',
		'desc'      => esc_html__( 'Show or hide the Header Banner Image (This applies to Default Template And Single Post Styles 1-5).','reendex' ),
		'type'      => 'radio_inline',
		'default'   => 'hide',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	$header_banner_option->add_field( array(
		'name'      => esc_html__( 'Header Image', 'reendex' ),
		'id'        => $prefix . 'header_image',
		'desc'      => esc_html__( 'Check To Use Featured Image As Banner Image.','reendex' ),
		'type'      => 'radio_inline',
		'default'   => 'no',
		'options'   => array(
			'yes'   => esc_html__( 'Use Featured Image on Banner', 'reendex' ),
			'no'    => esc_html__( 'Use Default Image (from Customizer) on Banner', 'reendex' ),
		),
	));

	$header_banner_option->add_field( array(
		'name'       => esc_html__( 'Show/Hide Banner Title', 'reendex' ),
		'id'         => $prefix . 'post_banner_title_show',
		'desc'       => esc_html__( 'Show or hide Header Banner Title (This applies to Default Template And Single Post Styles 1-5).','reendex' ),
		'type'       => 'radio_inline',
		'default'    => 'hide',
		'options'    => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	$header_banner_option->add_field( array(
		'name'      => esc_html__( 'Banner Title Text', 'reendex' ),
		'desc'      => esc_html__( 'Add Banner Title in this field (This applies to Default Template And Single Post Styles 1-5).', 'reendex' ),
		'id'        => $prefix . 'post_banner_title_text',
		'default'   => '',
		'type'      => 'text',
	) );

	$header_banner_option->add_field( array(
		'name'      => esc_html__( 'Breadcrumbs', 'reendex' ),
		'id'        => $prefix . 'page_breadcrumbs',
		'desc'      => esc_html__( 'You can show or hide the Breadcrumbs on this page.','reendex' ),
		'type'      => 'radio_inline',
		'default'   => 'hide',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	// Page Breadcrumb.
	$page_breadcrumb_option = new_cmb2_box( array(
		'id'	        => $prefix . 'page_breadcrumb_option',
		'title'	        => esc_html__( 'Page Breadcrums Settings', 'reendex' ),
		'object_types'  => array( 'page' ), // Post type.
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left.
	));
	$page_breadcrumb_option->add_field( array(
		'name'      => esc_html__( 'Breadcrumbs', 'reendex' ),
		'id'        => $prefix . 'page_breadcrumbs',
		'desc'      => esc_html__( 'You can show or hide the Breadcrumbs on this page.','reendex' ),
		'type'      => 'radio_inline',
		'default'   => 'hide',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	// Single Post Social Meta.
	$social_meta_option = new_cmb2_box( array(
		'id'	        => $prefix . 'social_meta_option',
		'title'	        => esc_html__( 'Social Meta Settings', 'reendex' ),
		'object_types'  => array( 'post' ), // Post type.
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true,
		'closed'        => true,
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Show/Hide All Share Icons and Category Tag', 'reendex' ),
		'id'        => $prefix . 'share_all',
		'desc'      => esc_html__( 'Note: Category Tag Is Available only on Posts Styles 3/4/7','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Facebook Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_facebook',
		'desc'      => esc_html__( 'Show/Hide Facebook Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Twitter Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_twitter',
		'desc'      => esc_html__( 'Show/Hide Twitter Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Google Plus Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_google',
		'desc'      => esc_html__( 'Show/Hide Google Plus Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Linkedin Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_linkedin',
		'desc'      => esc_html__( 'Show/Hide Linkedin Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Pinterest Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_pinterest',
		'desc'      => esc_html__( 'Show/Hide Pinterest Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Tumblr Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_tumblr',
		'desc'      => esc_html__( 'Show/Hide Tumblr Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Reddit Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_reddit',
		'desc'      => esc_html__( 'Show/Hide Reddit Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Stumbleupon Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_stumbleupon',
		'desc'      => esc_html__( 'Show/Hide Stumbleupon Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Digg Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_digg',
		'desc'      => esc_html__( 'Show/Hide Digg Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Vk Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_vk',
		'desc'      => esc_html__( 'Show/Hide Vk Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));
	$social_meta_option->add_field( array(
		'name'      => esc_html__( 'Pocket Share Icon', 'reendex' ),
		'id'        => $prefix . 'share_pocket',
		'desc'      => esc_html__( 'Show/Hide Pocket Share Icon','reendex' ),
		'type'      => 'radio_inline',
		'options'   => array(
			'show'   => esc_html__( 'Show', 'reendex' ),
			'hide'   => esc_html__( 'Hide', 'reendex' ),
		),
	));

	// Metabox for Video post format.
	$video_post_options = new_cmb2_box( array(
		'id'                => $prefix . '_reendex_video_format',
		'title'             => esc_html__( 'Video Post Options', 'reendex' ),
		'object_types'      => array( 'post' ), // Post type.
		'show_on'           => array(
			'key' => 'post_format',
			'value' => 'video',
		),
		'context'           => 'side',
		'priority'          => 'default',
		'show_names'        => true, // Show field names on the left.
	) );
	$video_post_options->add_field( array(
		'name'             => esc_html__( 'Video Url', 'reendex' ),
		'id'               => $prefix . 'post_formate_vdo',
		'desc'             => esc_html__( 'Add the video url here. Examples: https://youtu.be/V4LS_kJdIes https://vimeo.com/29794566', 'reendex' ),
		'type'             => 'textarea_small',
	) );

	// Metabox for Facebook Video post format.
	$facebook_video_post_options = new_cmb2_box( array(
		'id'                => $prefix . '_reendex_video_format',
		'title'             => esc_html__( 'Facebook Video', 'reendex' ),
		'object_types'      => array( 'post' ), // Post type.
		'show_on'           => array(
			'key' => 'post_format',
			'value' => 'video',
		),
		'context'           => 'side',
		'priority'          => 'default',
		'show_names'        => true, // Show field names on the left.
	) );
	$facebook_video_post_options->add_field( array(
		'name'             => esc_html__( 'Facebook Video Url', 'reendex' ),
		'id'               => $prefix . 'facebook_video',
		'desc'             => esc_html__( 'Add Facebook video url here.', 'reendex' ),
		'type'             => 'textarea_small',
	) );

	// Metabox for Audio post format.
	$audio_post_options = new_cmb2_box( array(
		'id'                => $prefix . '_reendex_audio_format',
		'title'             => esc_html__( 'Audio Post Options', 'reendex' ),
		'object_types'      => array( 'post' ), // Post type.
		'show_on'           => array(
			'key' => 'post_format',
			'value' => 'audio',
		),
		'context'           => 'side',
		'priority'          => 'default',
		'show_names'        => true, // Show field names on the left.
	) );
	$audio_post_options->add_field( array(
		'name'             => esc_html__( 'Audio File URL', 'reendex' ),
		'id'               => $prefix . 'post_formate_audio',
		'desc'             => esc_html__( 'Add the audio url here. A self hosted or an external audio url can be added.', 'reendex' ),
		'type'             => 'textarea_small',
	) );

	// Meta Box for video.
	$video = new_cmb2_box(array(
		'id'               => $prefix . 'video',
		'title'            => esc_html__( 'Aditional video area', 'reendex' ),
		'desc'             => esc_html__( 'Video area', 'reendex' ),
		'object_types'     => array( 'our-video' ), // Post type.
	));
	$video->add_field(array(
		'id'               => $prefix . 'video_link',
		'name'             => esc_html__( 'Video Link','reendex' ),
		'desc'             => esc_html__( 'Video Link', 'reendex' ),
		'type'             => 'text',
	));
	$video->add_field(array(
		'id'               => $prefix . 'video_completed',
		'name'             => esc_html__( 'Completed Date','reendex' ),
		'desc'             => esc_html__( 'Completed Date', 'reendex' ),
		'type'             => 'text_date',
		'date_format'      => 'd M Y',
	));
	$video->add_field(array(
		'id'               => $prefix . 'client_name',
		'name'             => esc_html__( 'Client Name','reendex' ),
		'desc'             => esc_html__( 'Client name', 'reendex' ),
		'type'             => 'text',
	));
	$video->add_field(array(
		'id'               => $prefix . 'client_link',
		'name'             => esc_html__( 'Client Link','reendex' ),
		'desc'             => esc_html__( 'Client Link', 'reendex' ),
		'type'             => 'text',
	));

	$video_group_field = $video->add_field(array(
		'id'               => $prefix . 'video_group',
		'type'             => 'group',
		'description'      => esc_html__( 'Video Skill', 'reendex' ),
		'options' => array(
			'group_title'   => esc_html__( 'Skill No : {#}','reendex' ),
			'add_button'    => esc_html__( 'Add Skill','reendex' ),
			'remove_button' => esc_html__( 'Remove Skill' , 'reendex' ),
			'sortable'      => true,
		),
	));
	$video->add_group_field( $video_group_field, array(
		'name'              => esc_html__( 'Skill Name','reendex' ),
		'id'                => $prefix . 'skill_name',
		'type'              => 'text',
	));

	// Social media.
	$social_group_field = $video->add_field(array(
		'id'                => $prefix . 'social_group',
		'type'              => 'group',
		'description'       => esc_html__( 'Our client social icon', 'reendex' ),
		'options' => array(
			'group_title'   => esc_html__( 'Social Media {#}','reendex' ),
			'add_button'    => esc_html__( 'Add field','reendex' ),
			'remove_button' => esc_html__( 'Remove' , 'reendex' ),
			'sortable'      => true,
		),
	));
	$video->add_group_field( $social_group_field, array(
		'name'  => esc_html__( 'Social Icon','reendex' ),
		'id'    => $prefix . 'social_icon',
		'type'  => 'text',
	));
	$video->add_group_field( $social_group_field, array(
		'name'  => esc_html__( 'Social Link','reendex' ),
		'id'    => $prefix . 'social_link',
		'type'  => 'text',
	));

	// Meta Box for TV Schedule.
	$tvschedule = new_cmb2_box(array(
		'id'            => $prefix . 'tvschedule',
		'title'         => esc_html__( 'Aditional TV Schedule area', 'reendex' ),
		'desc'          => esc_html__( 'Video area', 'reendex' ),
		'object_types'  => array( 'tv_schedule' ), // Post type.
	));
	$tvschedule->add_field(array(
		'id'    => $prefix . 'tvschedule_shift',
		'name'  => esc_html__( 'TV Schedule Shift','reendex' ),
		'desc'  => esc_html__( 'TV Schedule Shift', 'reendex' ),
		'type'  => 'radio_inline',
		 'options' => array(
			'morning' 	=> esc_html__( 'Morning', 'reendex' ),
			'afternoon' => esc_html__( 'Afternoon', 'reendex' ),
			'evening'   => esc_html__( 'Evening', 'reendex' ),
		),
	));
	$tvschedule->add_field(array(
		'id'    => $prefix . 'tvschedule_subtitle',
		'name'  => esc_html__( 'Sub Title','reendex' ),
		'desc'  => esc_html__( 'Sub Title', 'reendex' ),
		'type'  => 'text',
	));
	$tvschedule->add_field(array(
		'id'            => $prefix . 'tvschedule_date',
		'name'          => esc_html__( 'Schedule Date','reendex' ),
		'desc'          => esc_html__( 'Schedule Date', 'reendex' ),
		'type'          => 'text_date',
		'date_format'   => 'd/m/Y',
	));
	$tvschedule->add_field(array(
		'id'            => $prefix . 'tvschedule_time',
		'name'          => esc_html__( 'Schedule Time','reendex' ),
		'desc'          => esc_html__( 'Schedule Time', 'reendex' ),
		'type'          => 'text_time',
		'time_format'   => 'G:i',
	));

	// Meta Box for Our team.
	$ourteam = new_cmb2_box(array(
		'id'            => $prefix . 'ourteam',
		'title'         => esc_html__( 'Additional Team area', 'reendex' ),
		'desc'          => esc_html__( 'Team area', 'reendex' ),
		'object_types'  => array( 'our_team' ), // Post type.
	));

	$ourteam->add_field(array(
		'id'	=> $prefix . 'destination',
		'name'	=> esc_html__( 'Team Member Job','reendex' ),
		'desc'	=> esc_html__( 'Team Member Job','reendex' ),
		'type'  => 'text',
	));
	$ourteam->add_field(array(
		'id'	=> $prefix . 'twiter_user',
		'name'	=> esc_html__( 'Team Member Twitter Name','reendex' ),
		'desc'	=> esc_html__( 'Team Member Twitter Name','reendex' ),
		'type'  => 'text',
	));
	$ourteam->add_field(array(
		'id'	=> $prefix . 'twiter_link',
		'name'	=> esc_html__( 'Team Member Twitter Link','reendex' ),
		'desc'  => esc_html__( 'Team Member Twitter Link','reendex' ),
		'type'  => 'text',
	));
}



/**
 * Create a metabox to add some custom fields in posts.
 *
 * @package Reendex
 */

add_action( 'add_meta_boxes', 'reendex_post_settings_box' );

/**
 * Add the Meta Box.
 *
 * @since 1.0.0
 */
function reendex_post_settings_box() {
	add_meta_box(
		'reendex_post_settings_box',
		esc_html__( 'Post settings', 'reendex' ),
		'reendex_post_settings',
		'post',
		'normal',
		'high'
	);
}

/**
 * Render theme settings meta box.
 *
 * @since 1.0.
 */
function reendex_post_settings() {
	global $post, $reendex_post_template_layout, $reendex_sidebar_layout ;
	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'reendex_post_settings', 'reendex_post_settings_nonce' );
	?>
	<div class="my_post_settings">
		<table class="form-table">
			<tr>
				<td colspan="4"><?php esc_html_e( 'Post Header Layout', 'reendex' ); ?></td>
			</tr>		               
			<tr>
				<td>
					<?php
					foreach ( $reendex_post_template_layout as $field ) {
						$reendex_post_template_metalayout = get_post_meta( $post->ID, 'reendex_post_template_layout', true );?>
						<div class="radio-post-template-wrapper" available="<?php echo esc_attr( $field['available'] );?>" style="float:left; margin-right:30px;">
							<label class="description">
								<span><img src="<?php echo esc_url( $field['thumbnail'] ); ?>" alt="<?php echo esc_attr( $field['label'] ); ?>" /></span></br>
								<input type="radio" name="reendex_post_template_layout" value="<?php echo esc_attr( $field['value'] ); ?>"
									<?php checked( $field['value'], $reendex_post_template_metalayout );
									if ( empty( $reendex_post_template_metalayout ) && 'single' == $field['value'] ) {
										echo "checked='checked'";
									} ?>/>
								&nbsp;<?php echo esc_html( $field['label'] ); ?>
							</label>
						</div><!-- /.radio-post-template-wrapper -->
					<?php } // End foreach().
					?>
					<div class="clear"></div>
				</td>
			</tr>
		</table><!-- /.form-table -->
		<table class="form-table">
			<tr>
				<td colspan="4"><?php esc_html_e( 'Post Sidebar Layout', 'reendex' ); ?></td>
			</tr>            
			<tr>
				<td>
					<?php
					foreach ( $reendex_sidebar_layout as $field ) {
						$reendex_sidebar_metalayout = get_post_meta( $post->ID, 'reendex_sidebar_layout', true ); ?>
						<div class="radio-image-wrapper" style="float:left; margin-right:30px;">
							<label class="description">
								<span><img src="<?php echo esc_url( $field['thumbnail'] ); ?>" alt="<?php echo esc_attr( $field['label'] ); ?>" /></span></br>
								<input type="radio" name="reendex_sidebar_layout" value="<?php echo esc_attr( $field['value'] ); ?>"
									<?php checked( $field['value'], $reendex_sidebar_metalayout );
									if ( empty( $reendex_sidebar_metalayout ) && '' == $field['value'] ) {
										echo "checked='checked'";} ?>/>
									&nbsp;<?php echo esc_html( $field['label'] ); ?>
							</label>
						</div><!-- /.radio-image-wrapper -->
					<?php } // End foreach().
					?>
					<div class="clear"></div>
				</td>
			</tr>
		</table><!-- /.form-table -->
	</div><!-- /.my_post_settings -->
	<?php
}

	/**
	 * Adding the sidebar display of the meta option in the editor.
	 */
	$reendex_sidebar_layout = array(
		'leftsidebar'   => array(
			'value'     => 'leftsidebar',
			'label'     => esc_html__( 'Left sidebar', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/left-sidebar.png',
		),
		'rightsidebar'  => array(
			'value'     => 'rightsidebar',
			'label'     => esc_html__( 'Right sidebar (default)', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/right-sidebar.png',
		),
		'nosidebar'     => array(
			'value'     => 'nosidebar',
			'label'     => esc_html__( 'No sidebar', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/no-sidebar.png',
		),
	);

	/**
	 * Adding the post template layout display of the meta option in the editor.
	 */
	$reendex_post_template_layout = array(
		'default-template' => array(
			'value'     => 'single',
			'label'     => esc_html__( 'Default Template', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-default-style.png',
		),
		'style1-template' => array(
			'value'     => 'single-style1',
			'label'     => esc_html__( 'Style 1', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style1.png',
		),
		'style2-template' => array(
			'value'     => 'single-style2',
			'label'     => esc_html__( 'Style 2', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style2.png',
		),
		'style3-template' => array(
			'value'     => 'single-style3',
			'label'     => esc_html__( 'Style 3', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style3.png',
		),
		'style4-template' => array(
			'value'     => 'single-style4',
			'label'     => esc_html__( 'Style 4', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style4.png',
		),
		'style5-template' => array(
			'value'     => 'single-style5',
			'label'     => esc_html__( 'Style 5', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style5.png',
		),
		'style6-template' => array(
			'value'     => 'single-style6',
			'label'     => esc_html__( 'Style 6', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style6.png',
		),
		'style7-template' => array(
			'value'     => 'single-style7',
			'label'     => esc_html__( 'Style 7', 'reendex' ),
			'thumbnail' => get_template_directory_uri() . '/img/post-style7.png',
		),
	);

	/**
	 * Save theme settings meta box value.
	 *
	 * @since 1.0.
	 *
	 * @param int $post_id Post ID.
	 */
	function reendex_save_post_settings( $post_id ) {
		global $reendex_post_template_layout, $reendex_sidebar_layout, $post;

		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set before proceeding.
		if ( ! isset( $_POST['reendex_post_settings_nonce'] ) ) {
			return $post_id;
		}

		$nonce = sanitize_key( $_POST['reendex_post_settings_nonce'] );

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'reendex_post_settings' ) ) {
			return $post_id;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' === $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			}
		} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		foreach ( $reendex_post_template_layout as $field ) {
			// Execute this saving function.
			if ( isset( $_POST['reendex_post_template_layout'] ) ) {
				$new = sanitize_text_field( wp_unslash( $_POST['reendex_post_template_layout'] ) );
				update_post_meta( $post_id, 'reendex_post_template_layout', $new );
			}
		} // End foreach().

		foreach ( $reendex_sidebar_layout as $field ) {
			// Execute this saving function.
			if ( isset( $_POST['reendex_sidebar_layout'] ) ) {
				$new = sanitize_text_field( wp_unslash( $_POST['reendex_sidebar_layout'] ) );
				update_post_meta( $post_id, 'reendex_sidebar_layout', $new );
			}
		} // End foreach().
	}
	add_action( 'save_post', 'reendex_save_post_settings' );
