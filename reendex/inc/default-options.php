<?php
/**
 * Implement Default Theme/Customizer Options
 *
 * @package Reendex
 */

/**
 *  Default Theme layout options
 *
 * @param null
 * @return array $reendex_theme_layout
 */
if ( ! function_exists( 'reendex_get_default_theme_options' ) ) :
	/**
	 * Returns the default options.
	 */
	function reendex_get_default_theme_options() {

		$default_theme_options = array(

			/* Top Bar Options */
			'reendex_show_search_in_header'      => true,

			/* Date / Time Options */
			'reendex_time_format'   => esc_html__( 'g:i A', 'reendex' ),
			'reendex_date_format'   => esc_html__( 'D, F j, Y', 'reendex' ),

			/* Social Media */
			'reendex_facebook_link'     => '',
			'reendex_twitter_link'      => '',
			'reendex_googleplus_link'   => '',
			'reendex_linkedin_link'     => '',
			'reendex_pinterest_link'    => '',
			'reendex_tumblr_link'       => '',
			'reendex_reddit_link'       => '',
			'reendex_stumbleupon_link'  => '',
			'reendex_digg_link'         => '',
			'reendex_vimeo_link'        => '',
			'reendex_youtube_link'      => '',
			'reendex_rss_link'          => '',
			'reendex_instagram_link'    => '',

			/* Footer Options */
			'reendex_copyright_text' => esc_html__( 'Copyright &copy; All rights reserved.', 'reendex' ),

			/* Single Post Options */
			'reendex_random_posts_title'             => esc_html__( 'Reendex', 'reendex' ),
			'reendex_random_posts_subtitle'          => esc_html__( 'Must see news', 'reendex' ),
			'reendex_author_related_title'           => esc_html__( 'More Articles', 'reendex' ),
			'reendex_author_related_subtitle'        => esc_html__( 'By the same author', 'reendex' ),
			'reendex_related_category_title'         => esc_html__( 'Related Articles', 'reendex' ),
			'reendex_related_category_subtitle'      => esc_html__( 'From the same category', 'reendex' ),
			'reendex_related_category_number'        => '5',
			'reendex_author_related_number'          => '9',
			'reendex_single_ticker_content'          => '10',
			'reendex_related_content_length'         => '160',
			'reendex_random_posts_content_length'    => '40',
			'reendex_author_related_content_length'  => '160',
			'reendex_random_posts_per_page'          => '3',
			'reendex_single_disable_postmeta'        => 'disable_postmeta',

			/* Archive Page Options */
			'reendex_readmore_text'         => esc_html__( 'Read More', 'reendex' ),
			'reendex_disable_postmeta'      => 'disable_postmeta',
			'reendex_disable_post_footer'   => 'disable_post_footer',
			'reendex_excerpt_length'        => '100',

			/* Tag Archive Options */
			'reendex_tag_readmore_text'         => esc_html__( 'Read More', 'reendex' ),
			'reendex_tag_disable_postmeta'      => 'disable_postmeta',
			'reendex_tag_disable_post_footer'   => 'disable_post_footer',
			'reendex_tag_excerpt_length'        => '100',

			/* Category Archive Options */
			'reendex_category_readmore_text'         => esc_html__( 'Read More', 'reendex' ),
			'reendex_category_disable_postmeta'      => 'disable_postmeta',
			'reendex_category_disable_post_footer'   => 'disable_post_footer',
			'reendex_category_excerpt_length'        => '100',

			/* Search Results Options */
			'reendex_search_readmore_text'         => esc_html__( 'Read More', 'reendex' ),
			'reendex_search_disable_postmeta'      => 'disable_postmeta',
			'reendex_search_disable_post_footer'   => 'disable_post_footer',
			'reendex_search_excerpt_length'        => '100',

			/* Blog Page Options */
			'reendex_banner_subtitle_link'       => '',
			'reendex_blog_readmore_text'         => esc_html__( 'Read More', 'reendex' ),
			'reendex_blog_disable_postmeta'      => 'disable_postmeta',
			'reendex_blog_disable_post_footer'   => 'disable_post_footer',
			'reendex_blog_excerpt_length'        => '100',

			/* Coming Soon Page Options */
			'reendex_comingsoon_date'               => esc_html__( '12/25/2018', 'reendex' ),
			'reendex_comingsoon_socialmedia_title'  => esc_html__( 'Stay Connected', 'reendex' ),

			/* Video Page Options */
			'reendex_video-options'                 => 'normal',
			'reendex_gallery_title'                 => esc_html__( 'Must see videos', 'reendex' ),
			'reendex_video_categories'              => esc_html__( '765', 'reendex' ),
			'reendex_related_video_title'           => esc_html__( 'Related Videos', 'reendex' ),
			'reendex_related_video_subtitle'        => esc_html__( 'You might like', 'reendex' ),
			'reendex_video_related_number'          => '8',
			'reendex_related_video_content_length'  => '70',

			/* Contact Page Options */
			'reendex_contact_page_ab_title'         => esc_html__( 'About Us', 'reendex' ),
			'reendex_contact_aboutus_text'          => esc_html__( 'When you are building a website, it is tempting to get distracted by all the bells and whistles of the design process and forget all about creating compelling content. But having awesome content on your website is crucial to making inbound marketing work for your business. We know ... easier said than done.', 'reendex' ),
			'reendex_contact_page_info_title'       => esc_html__( 'Contact Information', 'reendex' ),
			'reendex_our_address'                   => esc_html__( '121 King Street, Melbourne, Australia', 'reendex' ),
			'reendex_contact'                       => esc_html__( '+00 (123) 456 7890', 'reendex' ),
			'reendex_email'                         => esc_html__( 'info@domain.com', 'reendex' ),
			'reendex_contact_map_title'             => esc_html__( 'Find Us On Map', 'reendex' ),
			'reendex_google_maps_api_key'           => esc_html__( 'AIzaSyBQ5pstmKdT29xKaaP39CLlTPHN-kgthb4', 'reendex' ),
			'reendex_google_address'                => esc_html__( '121KingStreetMelbourneAustralia', 'reendex' ),
			'reendex_contact_form_title'            => esc_html__( 'Contact Form', 'reendex' ),
			'reendex_gmap_zoom'                     => '17',
			'reendex_contact_banner_subtitle_link'  => '',

			/* Weather Options */
			'reendex_wea_api'               => esc_html__( 'Add your Weather API Key here', 'reendex' ),
			'reendex_weather_location'      => esc_html__( 'New York', 'reendex' ),
			'reendex_weather_access_key'    => esc_html__( 'Add your Weather Access Key here', 'reendex' ),
		);

		return apply_filters( 'reendex_default_theme_options', $default_theme_options );
	}
endif;
