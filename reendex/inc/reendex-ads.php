<?php
/**
 * Template part for displaying Ads.
 *
 * @package Reendex
 */

/**
 *
 * Show Header Ads.
 */
function reendex_header_ad() {
	// Header Ad.
	$header_ad_image = get_theme_mod( 'reendex_header_ad_image' );
	$header_ad_url = get_theme_mod( 'reendex_header_ad_url' );
	$header_ad_target = get_theme_mod( 'reendex_header_ad_target', '_blank' );
	$header_attachment_url = attachment_url_to_postid( $header_ad_image );
	$header_image_alt = get_post_meta( $header_attachment_url, '_wp_attachment_image_alt', true );
	$header_ad_type = get_theme_mod( 'reendex_header_ad_type' );
	$header_ad_script = get_theme_mod( 'reendex_header_script_ad' );
	$header_ad_google_publisher_id = get_theme_mod( 'reendex_header_ad_publisher_id' );
	$header_ad_google_slot_id = get_theme_mod( 'reendex_header_ad_slot_id' );
	$header_ad_google_desktop_size = get_theme_mod( 'reendex_header_ad_desktop_size', 'auto' );
	$header_ad_shortcode_ad = get_theme_mod( 'reendex_header_ad_shortcode' );
	$header_ad_bottom_text = get_theme_mod( 'header_ad_bottom_text' );
	$ad_style = '';
	$random_string = reendex_get_random_string( 6 );
	$render = '';
	if ( 'image' == $header_ad_type ) { ?>
		<div class="header-ad-place">
			<?php if ( is_active_sidebar( 'header_ad_logo' ) ) : ?>
				<div id="header_add_right" class="add_header_right" role="complementary">
					<?php dynamic_sidebar( 'header_ad_logo' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>


<div class="desktop-ad">
				<div class="header-ad">
					<a target="<?php echo esc_attr( $header_ad_target ); ?>" href="<?php echo esc_url( $header_ad_url ); ?>"><img src="<?php echo esc_url( $header_ad_image ); ?>" alt="<?php echo esc_attr( $header_image_alt ); ?>" /></a>
					</div><!-- /.header-ad -->	
				</div><!-- /.desktop-ad -->
				<?php if ( '' != isset( $header_ad_bottom_text ) && $header_ad_bottom_text ) { ?>
					<div class='ad-bottom-text'>
						<?php echo esc_html( $header_ad_bottom_text ); ?>
					</div>
				<?php } ?>
			</div><!-- /.header-ad-place -->
	<?php } elseif ( 'code' == $header_ad_type ) { ?>
		<div class="header-ad-place">
			<div class="desktop-ad">
				<div class="header-ad">
					<?php
						add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
						echo wp_kses( $header_ad_script, reendex_banner_allowed_html() );
					?>
				</div><!-- /.header-ad -->	
			</div><!-- /.desktop-ad -->
			<?php if ( '' != isset( $header_ad_bottom_text ) && $header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.header-ad-place -->
	<?php } elseif ( 'googleads' == $header_ad_type ) { ?>
		<div class="header-ad-place">
			<div class="desktop-ad">
				<div class="header-ad">
					<?php
					$desktop_ad_size = array( '728','90' );
					if ( 'auto' !== $header_ad_google_desktop_size ) {
						$desktop_ad_size = explode( 'x', $header_ad_google_desktop_size );
					}
					// Show ad on desktop.
					if ( 'hide' !== $header_ad_google_desktop_size && is_array( $desktop_ad_size ) && isset( $desktop_ad_size['0'] ) && isset( $desktop_ad_size['1'] ) ) {
						$ad_style .= "@media (min-width:1025px) { .ad_{$random_string}{ width:{$desktop_ad_size[0]}px !important; height:{$desktop_ad_size[1]}px !important; }\n";
					}
					$desktop_hide_class = '';
					if ( 'hide' == $header_ad_google_desktop_size ) {
						$desktop_hide_class = 'desktop-ad-hide';
					}
					$render = "
					<style type='text/css'>{$ad_style}</style>
					<div class=\"$desktop_hide_class ad-inner-wrapper\">
							<ins class=\"adsbygoogle ad_{$random_string}\"
							style=\"display:inline-block;\"
							data-ad-client=\"$header_ad_google_publisher_id\"
							data-ad-slot=\"$header_ad_google_slot_id\"
							></ins>
					</div>";
					add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
					echo wp_kses( $render, reendex_banner_allowed_html() );
					?>
				</div><!-- /.header-ad -->	
			</div><!-- /.desktop-ad -->
			<?php if ( '' != isset( $header_ad_bottom_text ) && $header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.header-ad-place -->
	<?php } else { ?>
		<div class="header-ad-place">
			<div class="desktop-ad">
				<div class="header-ad">
					<?php
						$render = "<div class='ads_shortcode'>" . do_shortcode( $header_ad_shortcode_ad ) . '</div>';
						add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
						echo wp_kses( $render, reendex_banner_allowed_html() );
					?>
				</div><!-- /.header-ad -->	
			</div><!-- /.desktop-ad -->
			<?php if ( '' != isset( $header_ad_bottom_text ) && $header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.header-ad-place -->
	<?php } // End if().
}
add_action( 'reendex_header_ad', 'header_ad' );
/**
 * Header Ad Callback.
 */
function header_ad() {
	echo wp_kses( reendex_header_ad( 'header_ad' ), reendex_banner_allowed_html() );
}

/**
 *
 * Show Above Header Ads.
 */
function reendex_above_header_ad() {
	// Header Ad.
	$above_header_ad_image = get_theme_mod( 'reendex_above_header_ad_image' );
	$above_header_ad_url = get_theme_mod( 'reendex_above_header_ad_url' );
	$above_header_ad_target = get_theme_mod( 'reendex_above_header_ad_target', '_blank' );
	$above_header_attachment_url = attachment_url_to_postid( $above_header_ad_image );
	$above_header_image_alt = get_post_meta( $above_header_attachment_url, '_wp_attachment_image_alt', true );
	$above_header_ad_type = get_theme_mod( 'reendex_above_header_ad_type' );
	$above_header_ad_script = get_theme_mod( 'reendex_above_header_script_ad' );
	$above_header_ad_google_publisher_id = get_theme_mod( 'reendex_above_header_ad_publisher_id' );
	$above_header_ad_google_slot_id = get_theme_mod( 'reendex_above_header_ad_slot_id' );
	$above_header_ad_google_desktop_size = get_theme_mod( 'reendex_above_header_ad_desktop_size', 'auto' );
	$above_header_ad_google_tablet_size = get_theme_mod( 'reendex_above_header_ad_tablet_size', 'auto' );
	$above_header_ad_google_phone_size = get_theme_mod( 'reendex_above_header_ad_phone_size', 'auto' );
	$above_header_ad_shortcode_ad = get_theme_mod( 'reendex_above_header_ad_shortcode' );
	$above_header_ad_bottom_text = get_theme_mod( 'above_header_ad_bottom_text' );
	$ad_style = '';
	$random_string = reendex_get_random_string( 6 );
	$render = '';
	if ( 'image' == $above_header_ad_type ) { ?>
		<div class="above-header-ad-wrapper">
			<a target="<?php echo esc_attr( $above_header_ad_target ); ?>" href="<?php echo esc_url( $above_header_ad_url ); ?>"><img src="<?php echo esc_url( $above_header_ad_image ); ?>" alt="<?php echo esc_attr( $above_header_image_alt ); ?>" /></a>
			<?php if ( '' != isset( $above_header_ad_bottom_text ) && $above_header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $above_header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.above-header-ad-wrapper -->
	<?php } elseif ( 'code' == $above_header_ad_type ) { ?>
		<div class="above-header-ad-wrapper">
			<?php
				add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
				echo wp_kses( $above_header_ad_script, reendex_banner_allowed_html() );
			if ( '' != isset( $above_header_ad_bottom_text ) && $above_header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $above_header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.above-header-ad-wrapper -->
	<?php } elseif ( 'googleads' == $above_header_ad_type ) { ?>
		<div class="above-header-ad-wrapper">
			<?php
			$desktop_ad_size = array( '970','90' );
			$tablet_ad_size = array( '468','60' );
			$phone_ad_size = array( '320','50' );
			if ( 'auto' !== $above_header_ad_google_desktop_size ) {
				$desktop_ad_size = explode( 'x', $above_header_ad_google_desktop_size );
			}
			if ( 'auto' !== $above_header_ad_google_tablet_size ) {
				$tablet_ad_size = explode( 'x', $above_header_ad_google_tablet_size );
			}
			if ( 'auto' !== $above_header_ad_google_phone_size ) {
				$phone_ad_size = explode( 'x', $above_header_ad_google_phone_size );
			}
			// Show ad on desktop.
			if ( 'hide' !== $above_header_ad_google_desktop_size && is_array( $desktop_ad_size ) && isset( $desktop_ad_size['0'] ) && isset( $desktop_ad_size['1'] ) ) {
				$ad_style .= ".ad_{$random_string}{ width:{$desktop_ad_size[0]}px !important; height:{$desktop_ad_size[1]}px !important; }\n";
			}
			// Show ad on tablet.
			if ( 'hide' !== $above_header_ad_google_tablet_size && is_array( $tablet_ad_size ) && isset( $tablet_ad_size['0'] ) && isset( $tablet_ad_size['1'] ) ) {
				$ad_style .= "@media (max-width:1199px) { .ad_{$random_string}{ width:{$tablet_ad_size[0]}px !important; height:{$tablet_ad_size[1]}px !important; } }\n";
			}
			// Show ad on phone.
			if ( 'hide' !== $above_header_ad_google_phone_size && is_array( $phone_ad_size ) && isset( $phone_ad_size['0'] ) && isset( $phone_ad_size['1'] ) ) {
				$ad_style .= "@media (max-width:767px) { .ad_{$random_string}{ width:{$phone_ad_size[0]}px !important; height:{$phone_ad_size[1]}px !important; } }\n";
			}
			$desktop_hide_class = '';
			$tablet_hide_class = '';
			$phone_hide_class = '';
			if ( 'hide' == $above_header_ad_google_desktop_size ) {
				$desktop_hide_class = 'desktop-ad-hide';
			}
			if ( 'hide' == $above_header_ad_google_tablet_size ) {
				$tablet_hide_class = 'tablet-ad-hide';
			}
			if ( 'hide' == $above_header_ad_google_phone_size ) {
				$phone_hide_class = 'phone-ad-hide';
			}
			$render = "
			<style type='text/css'>{$ad_style}</style>
			<div class=\"$desktop_hide_class $tablet_hide_class $phone_hide_class ad-inner-wrapper\">
					<ins class=\"adsbygoogle ad_{$random_string}\"
					style=\"display:inline-block\"
					data-ad-client=\"$above_header_ad_google_publisher_id\"
					data-ad-slot=\"$above_header_ad_google_slot_id\"
					></ins>   
			</div>";
			add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
			echo wp_kses( $render, reendex_banner_allowed_html() );
			if ( '' != isset( $above_header_ad_bottom_text ) && $above_header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $above_header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.above-header-ad-wrapper -->
	<?php } else { ?>
		<div class="above-header-ad-wrapper">
			<?php
				$render = "<div class='ads_shortcode'>" . do_shortcode( $above_header_ad_shortcode_ad ) . '</div>';
				add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
				echo wp_kses( $render, reendex_banner_allowed_html() );
			if ( '' != isset( $above_header_ad_bottom_text ) && $above_header_ad_bottom_text ) { ?>
				<div class='ad-bottom-text'>
					<?php echo esc_html( $above_header_ad_bottom_text ); ?>
				</div>
			<?php } ?>
		</div><!-- /.above-header-ad-wrapper -->
	<?php } // End if().
}
add_action( 'reendex_above_header_ad', 'above_header_ad' );
/**
 * Header Ad Callback.
 */
function above_header_ad() {
	echo wp_kses( reendex_above_header_ad( 'above_header_ad' ), reendex_banner_allowed_html() );
}
