<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<div class="comment-title title-style01">
			<h3>
			<?php
				$comments_number = get_comments_number();
			if ( '1' === $comments_number ) {
				esc_html_x( 'One comment', 'comments title', 'reendex' );
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: number of comments */
					esc_html( _nx(
						'%s Comment',
						'%s Comments',
						$comments_number,
						'comments title',
						'reendex'
					) ),
					number_format_i18n( $comments_number )
				);
			}
			?>		
			</h3>
		</div><!-- /.comments-title -->
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-above" class="navigation comment-navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation','reendex' ); ?></h2>
			<div class="nav-links">
				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'reendex' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'reendex' ) ); ?></div>
			</div><!-- /.nav-links -->
		</nav><!-- /#comment-nav-above -->
		<?php endif; // Check for comment navigation. ?>
		<ul class="comments-list">
			<?php
				wp_list_comments( array(
					'style'         => 'ul',
					'short_ping'    => true,
					'avatar_size'   => 48,
					'callback'      => 'reendex_comment_reply',
				) );
			?>
		</ul><!-- /.comment-list -->
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation','reendex' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'reendex' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'reendex' ) ); ?></div>

			</div><!-- /.nav-links -->
		</nav><!-- /#comment-nav-below -->
		<?php endif; // Check for comment navigation.
	endif; // Check for have_comments().
	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.','reendex' ); ?></p>
		<?php endif;
	comment_form();
	?>
</div><!-- /#comments -->
