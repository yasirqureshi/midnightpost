<?php
/**
 * The template for displaying posts in the Video post format
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
															
	<div class="post post-full clearfix">
		<div class="entry-media">
			<div class="fitvids-video">	        
				<iframe src="<?php echo esc_url( get_post_meta( get_the_id(), 'reendex_post_formate_vdo', true ) ); ?>"></iframe>
			</div><!-- /.fitvids-video -->			
		</div><!-- /.entry-media -->
		<div class="entry-main">
			<div class="entry-title">
				<h4 class="entry-title"><?php the_title();?></h4>
			</div><!-- /.entry-title -->
			<div class="post-meta-elements">
				<div class="post-meta-author"> <i class="fa fa-user"></i><a href="<?php echo esc_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php esc_html_e( 'By','reendex' );?> <?php the_author(); ?></a> </div>
				<div class="post-meta-date">
					<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
						<i class="fa fa-calendar"></i><a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'd' ) ) ); ?>"><?php the_time( 'F d, Y','reendex' ); ?></a>
					<?php } else { ?>
						<i class="fa fa-calendar"></i><span class="date updated"><a href="<?php echo esc_url( get_day_link( get_the_date( 'Y' ), get_the_date( 'm' ), get_the_date( 'd' ) ) ) ?>"><?php echo esc_html( get_the_modified_date( 'F j, Y' ) ); ?></a></span>
					<?php } ?>
				</div><!-- /.post-meta-date -->
				<span class="post-meta-comments"> <i class="fa fa-comment-o"></i><?php comments_popup_link( esc_html__( 'Leave a comment', 'reendex' ), esc_html__( '1 Comment', 'reendex' ), esc_html__( '% Comments', 'reendex' ) ); ?></span>
			</div><!-- /.post-meta-elements -->
			<div class="entry-content">
				<?php the_content();?>
			</div><!-- /.entry-content -->
		</div><!-- /.entry-main -->
	</div><!-- /.post post-full clearfix -->

</article><!-- #post-## -->
