<?php
/**
 * Header style 5
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_top_menu' ) ) :
	/**
	 * Top Menu codes
	 */
	function reendex_top_menu() {
		?>
		<!-- Begin .top-menu -->                 
		<div class="top-menu header-style-five"> 
			<!-- Begin .container -->                     
			<div class="container">        
				<?php get_template_part( 'template-parts/content', 'topbar' );?>
			</div>                     
			<!-- End .container -->                     
		</div>                 
		<!-- End .top-menu -->
		<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_top_menu', 20 );

if ( ! function_exists( 'reendex_site_branding' ) ) :
	/**
	 * Site branding Logo
	 */
	function reendex_site_branding() {
	?>
		<div class="container header-style-five">
			<div class="logo-ad-wrapper clearfix">
				<?php
				get_template_part( 'template-parts/header/header', 'site-branding-logo' );
				get_template_part( 'template-parts/header/header', 'site-ad-banner' );
				?>
			</div><!-- /.logo-ad-wrapper clearfix -->
		</div><!-- /.container header-style-five -->
		<div class="container-fluid">
			<?php do_action( 'reendex_menu' ); ?>
		</div><!-- /.container-fluid -->
	<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_branding', 30 );

if ( ! function_exists( 'reendex_site_navigation' ) ) :
	/**
	 * Site navigation codes
	 */
	function reendex_site_navigation() {
	?>
	<div class="container-fluid header-style-five">	    
		<?php get_template_part( 'template-parts/header/header', 'main-menu' ); ?>
	</div><!-- /.container-fluid -->
	<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_navigation', 40 );
