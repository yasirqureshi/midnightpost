<?php
/**
 * Post meta: Publish and update date
 *
 * @package    Reendex
 *
 * @since    1.0
 */

if ( get_theme_mod( 'reendex_post_date_format' ) === 'ago' ) {
	if ( get_theme_mod( 'reendex_post_date' ) === 'published' ) : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Published:','reendex' ); ?>
			<span><?php echo esc_html( human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ) . esc_html__( ' ago', 'reendex' ); ?></span>
		</div>
	<?php elseif ( get_theme_mod( 'reendex_post_date' ) === 'updated' ) : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Updated:','reendex' ); ?>
			<span><?php echo esc_html( human_time_diff( get_the_modified_date( 'U' ), current_time( 'timestamp' ) ) ) . esc_html__( ' ago', 'reendex' ); ?></span>
		</div>
	<?php else : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Published:','reendex' ); ?>
			<span><?php echo esc_html( human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ) . esc_html__( ' ago', 'reendex' ); ?></span>
		</div>
		<div class="post-date"><?php echo esc_html__( 'Article Updated:','reendex' ); ?>
			<span><?php echo esc_html( human_time_diff( get_the_modified_date( 'U' ), current_time( 'timestamp' ) ) ) . esc_html__( ' ago', 'reendex' ); ?></span>
		</div>
	<?php endif;
} elseif ( get_theme_mod( 'reendex_post_date_format' ) === 'default' ) {
	if ( get_theme_mod( 'reendex_post_date' ) === 'published' ) : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Published:', 'reendex' ); ?>
			<span><?php echo esc_html( get_the_date() ); ?></span>
		</div>
	<?php elseif ( get_theme_mod( 'reendex_post_date' ) === 'updated' ) : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Updated:', 'reendex' ); ?>
			<span><?php echo esc_html( get_the_modified_date() ); ?></span>
		</div>
	<?php else : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Published:', 'reendex' ); ?>
			<span><?php echo esc_html( get_the_date() ); ?></span>
		</div>
		<div class="post-date"><?php echo esc_html__( 'Article Updated:', 'reendex' ); ?>
			<span><?php echo esc_html( get_the_modified_date() ); ?></span>
		</div>
	<?php endif;
} elseif ( get_theme_mod( 'reendex_post_date_format' ) === 'custom' ) {
	if ( get_theme_mod( 'reendex_post_date' ) === 'published' ) : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Published:', 'reendex' ); ?>
			<span><?php echo esc_html( date_i18n( get_theme_mod( 'reendex_custom_date_format' ), get_the_date( 'U' ) ) ); ?></span>
		</div>
	<?php elseif ( get_theme_mod( 'reendex_post_date' ) === 'updated' ) : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Updated:', 'reendex' ); ?>
			<span><?php echo esc_html( date_i18n( get_theme_mod( 'reendex_custom_date_format' ), get_the_modified_date( 'U' ) ) ); ?></span>
		</div>
	<?php else : ?>
		<div class="post-date"><?php echo esc_html__( 'Article Published:', 'reendex' ); ?>
			<span><?php echo esc_html( date_i18n( get_theme_mod( 'reendex_custom_date_format' ), get_the_date( 'U' ) ) ); ?></span>
		</div>
		<div class="post-date"><?php echo esc_html__( 'Article Updated:', 'reendex' ); ?>
			<span><?php echo esc_html( date_i18n( get_theme_mod( 'reendex_custom_date_format' ), get_the_modified_date( 'U' ) ) ); ?></span>
		</div>
	<?php endif;
} // End if().
