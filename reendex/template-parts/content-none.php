<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

?>

<section class="no-results not-found">
	<div class="module-title">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'reendex' ); ?></h1>
	</div><!-- /.module-title -->
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
			<p><?php printf( /* translators: %1$s : description of url */
				wp_kses( esc_html__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'reendex' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				),
			esc_url( admin_url( 'post-new.php' ) ) ); ?>
			</p>
		<?php elseif ( is_search() ) : ?>
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'reendex' ); ?></p>
			<?php
				get_search_form();
		else : ?>
			<p><?php esc_html_e( 'The content for this section is under process and will be available soon. Perhaps searching can help.', 'reendex' ); ?></p>
			<p><?php esc_html_e( 'Thanks,', 'reendex' ); ?></p>
			<p><?php esc_html_e( 'The Management,', 'reendex' ); ?></p>
			<p><?php esc_html_e( 'The Midnight Posts.', 'reendex' ); ?></p>
			<?php
				get_search_form();
		endif; ?>
	</div><!-- /.page-content -->
</section><!-- /.no-results -->
